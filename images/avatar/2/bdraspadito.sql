-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 26-04-2016 a las 16:07:00
-- Versión del servidor: 5.7.10-log
-- Versión de PHP: 5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bdraspadito`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `binnacle`
--

CREATE TABLE `binnacle` (
  `id` int(10) UNSIGNED NOT NULL,
  `usersystem` int(11) NOT NULL,
  `contenttype` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `action` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `details` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `developer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `useragent` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `card`
--

CREATE TABLE `card` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `limit` int(11) NOT NULL,
  `details` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `background` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `foreground` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `coin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `published` enum('1','0') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `updated_at` date NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `card`
--

INSERT INTO `card` (`id`, `name`, `category`, `limit`, `details`, `background`, `foreground`, `coin`, `published`, `updated_at`, `created_at`) VALUES
(1, 'ganate una vaca', 'Poker Online', 102, 'sadsads', 'a1.jpg', 'html_logo.png', 'coin.png', '1', '2016-04-26', '2016-04-26'),
(2, '5%', 'Las Vegas', 60, 'sdfsdf', '6vg52v.jpg', 'demo1-foreground.png', 'coin2.png', '1', '2016-04-26', '2016-04-26'),
(3, 'piakachu', 'Virtuales', 213, 'te ganas un pokemon', '14183927.jpg', 'demo1-foreground.png', 'coin2.png', '1', '2016-04-26', '2016-04-26'),
(4, 'Bean', 'Casino en vivo', 250, 'sdf', '14183927.jpg', 'demo1-foreground.png', 'coin.png', '1', '2016-04-26', '2016-04-26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cardselect`
--

CREATE TABLE `cardselect` (
  `id` int(10) UNSIGNED NOT NULL,
  `player` int(11) NOT NULL,
  `card` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `category`
--

CREATE TABLE `category` (
  `id` int(10) UNSIGNED NOT NULL,
  `categoryname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` date NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `category`
--

INSERT INTO `category` (`id`, `categoryname`, `updated_at`, `created_at`) VALUES
(1, 'Poker Online', '2016-04-25', '2016-04-25'),
(2, 'Juegos 777', '2016-04-25', '2016-04-25'),
(3, 'Casino en vivo', '2016-04-25', '2016-04-25'),
(4, 'Tragamonedas', '2016-04-25', '2016-04-25'),
(5, 'Las Vegas', '2016-04-25', '2016-04-25'),
(6, 'e-Games', '2016-04-25', '2016-04-25'),
(7, 'Parlay', '2016-04-25', '2016-04-25'),
(8, 'Caballos', '2016-04-25', '2016-04-25'),
(9, 'Virtuales', '2016-04-25', '2016-04-25'),
(10, 'srfdgdfgdfg', '2016-04-26', '2016-04-26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `country_isos`
--

CREATE TABLE `country_isos` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `iso` varchar(3) NOT NULL,
  `translation` text NOT NULL,
  `timezone` text NOT NULL,
  `published` enum('1','0') NOT NULL DEFAULT '1',
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `country_isos`
--

INSERT INTO `country_isos` (`id`, `name`, `iso`, `translation`, `timezone`, `published`, `created_at`, `updated_at`) VALUES
(1, 'Afghanistan (UTC+04:30)', 'AF ', '{"en":"Afghanistan (UTC+04:30)"}', 'Asia/Kabul', '1', '2016-01-22', '2016-01-22'),
(2, 'Albania (UTC+01:00)', 'AL ', '{"en":"Albania (UTC+01:00)"}', 'Europe/Tirane', '1', '2016-01-22', '2016-01-22'),
(3, 'Algeria (UTC+01:00)', 'DZ ', '{"en":"Algeria (UTC+01:00)"}', 'Africa/Algiers', '1', '2016-01-22', '2016-01-22'),
(4, 'American Samoa (UTC-11:00)', 'AS ', '{"en":"American Samoa (UTC-11:00)"}', 'Pacific/Pago_Pago', '1', '2016-01-22', '2016-01-22'),
(5, 'Andorra (UTC+01:00)', 'AD ', '{"en":"Andorra (UTC+01:00)"}', 'Europe/Andorra', '1', '2016-01-22', '2016-01-22'),
(6, 'Antarctica (UTC+08:00)', 'AQ ', '{"en":"Antarctica (UTC+08:00)"}', 'Antarctica/Casey', '1', '2016-01-22', '2016-01-22'),
(7, 'Antarctica (UTC+07:00)', 'AQ ', '{"en":"Antarctica (UTC+07:00)"}', 'Antarctica/Davis', '1', '2016-01-22', '2016-01-22'),
(8, 'Antarctica (UTC+10:00)', 'AQ ', '{"en":"Antarctica (UTC+10:00)"}', 'Antarctica/DumontDUrville', '1', '2016-01-22', '2016-01-22'),
(9, 'Antarctica (UTC+05:00)', 'AQ ', '{"en":"Antarctica (UTC+05:00)"}', 'Antarctica/Mawson', '1', '2016-01-22', '2016-01-22'),
(10, 'Antarctica (UTC+13:00)', 'AQ ', '{"en":"Antarctica (UTC+13:00)"}', 'Antarctica/McMurdo', '1', '2016-01-22', '2016-01-22'),
(11, 'Antarctica (UTC-03:00)', 'AQ ', '{"en":"Antarctica (UTC-03:00)"}', 'Antarctica/Palmer', '1', '2016-01-22', '2016-01-22'),
(12, 'Antarctica (UTC-03:00)', 'AQ ', '{"en":"Antarctica (UTC-03:00)"}', 'Antarctica/Rothera', '1', '2016-01-22', '2016-01-22'),
(13, 'Antarctica (UTC+03:00)', 'AQ ', '{"en":"Antarctica (UTC+03:00)"}', 'Antarctica/Syowa', '1', '2016-01-22', '2016-01-22'),
(14, 'Antarctica (UTC+00:00)', 'AQ ', '{"en":"Antarctica (UTC+00:00)"}', 'Antarctica/Troll', '1', '2016-01-22', '2016-01-22'),
(15, 'Antarctica (UTC+06:00)', 'AQ ', '{"en":"Antarctica (UTC+06:00)"}', 'Antarctica/Vostok', '1', '2016-01-22', '2016-01-22'),
(16, 'Antigua & Barbuda (UTC-04:00)', 'AG ', '{"en":"Antigua & Barbuda (UTC-04:00)"}', 'America/Antigua', '1', '2016-01-22', '2016-01-22'),
(17, 'Argentina (UTC-03:00)', 'AR ', '{"en":"Argentina (UTC-03:00)"}', 'America/Argentina/Buenos_Aires', '1', '2016-01-22', '2016-01-22'),
(18, 'Argentina (UTC-03:00)', 'AR ', '{"en":"Argentina (UTC-03:00)"}', 'America/Argentina/Catamarca', '1', '2016-01-22', '2016-01-22'),
(19, 'Argentina (UTC-03:00)', 'AR ', '{"en":"Argentina (UTC-03:00)"}', 'America/Argentina/Cordoba', '1', '2016-01-22', '2016-01-22'),
(20, 'Argentina (UTC-03:00)', 'AR ', '{"en":"Argentina (UTC-03:00)"}', 'America/Argentina/Jujuy', '1', '2016-01-22', '2016-01-22'),
(21, 'Argentina (UTC-03:00)', 'AR ', '{"en":"Argentina (UTC-03:00)"}', 'America/Argentina/La_Rioja', '1', '2016-01-22', '2016-01-22'),
(22, 'Argentina (UTC-03:00)', 'AR ', '{"en":"Argentina (UTC-03:00)"}', 'America/Argentina/Mendoza', '1', '2016-01-22', '2016-01-22'),
(23, 'Argentina (UTC-03:00)', 'AR ', '{"en":"Argentina (UTC-03:00)"}', 'America/Argentina/Rio_Gallegos', '1', '2016-01-22', '2016-01-22'),
(24, 'Argentina (UTC-03:00)', 'AR ', '{"en":"Argentina (UTC-03:00)"}', 'America/Argentina/Salta', '1', '2016-01-22', '2016-01-22'),
(25, 'Argentina (UTC-03:00)', 'AR ', '{"en":"Argentina (UTC-03:00)"}', 'America/Argentina/San_Juan', '1', '2016-01-22', '2016-01-22'),
(26, 'Argentina (UTC-03:00)', 'AR ', '{"en":"Argentina (UTC-03:00)"}', 'America/Argentina/San_Luis', '1', '2016-01-22', '2016-01-22'),
(27, 'Argentina (UTC-03:00)', 'AR ', '{"en":"Argentina (UTC-03:00)"}', 'America/Argentina/Tucuman', '1', '2016-01-22', '2016-01-22'),
(28, 'Argentina (UTC-03:00)', 'AR ', '{"en":"Argentina (UTC-03:00)"}', 'America/Argentina/Ushuaia', '1', '2016-01-22', '2016-01-22'),
(29, 'Armenia (UTC+04:00)', 'AM ', '{"en":"Armenia (UTC+04:00)"}', 'Asia/Yerevan', '1', '2016-01-22', '2016-01-22'),
(30, 'Australia (UTC+11:00)', 'AU ', '{"en":"Australia (UTC+11:00)"}', 'Antarctica/Macquarie', '1', '2016-01-22', '2016-01-22'),
(31, 'Australia (UTC+10:30)', 'AU ', '{"en":"Australia (UTC+10:30)"}', 'Australia/Adelaide', '1', '2016-01-22', '2016-01-22'),
(32, 'Australia (UTC+10:00)', 'AU ', '{"en":"Australia (UTC+10:00)"}', 'Australia/Brisbane', '1', '2016-01-22', '2016-01-22'),
(33, 'Australia (UTC+10:30)', 'AU ', '{"en":"Australia (UTC+10:30)"}', 'Australia/Broken_Hill', '1', '2016-01-22', '2016-01-22'),
(34, 'Australia (UTC+11:00)', 'AU ', '{"en":"Australia (UTC+11:00)"}', 'Australia/Currie', '1', '2016-01-22', '2016-01-22'),
(35, 'Australia (UTC+09:30)', 'AU ', '{"en":"Australia (UTC+09:30)"}', 'Australia/Darwin', '1', '2016-01-22', '2016-01-22'),
(36, 'Australia (UTC+08:45)', 'AU ', '{"en":"Australia (UTC+08:45)"}', 'Australia/Eucla', '1', '2016-01-22', '2016-01-22'),
(37, 'Australia (UTC+11:00)', 'AU ', '{"en":"Australia (UTC+11:00)"}', 'Australia/Hobart', '1', '2016-01-22', '2016-01-22'),
(38, 'Australia (UTC+10:00)', 'AU ', '{"en":"Australia (UTC+10:00)"}', 'Australia/Lindeman', '1', '2016-01-22', '2016-01-22'),
(39, 'Australia (UTC+11:00)', 'AU ', '{"en":"Australia (UTC+11:00)"}', 'Australia/Lord_Howe', '1', '2016-01-22', '2016-01-22'),
(40, 'Australia (UTC+11:00)', 'AU ', '{"en":"Australia (UTC+11:00)"}', 'Australia/Melbourne', '1', '2016-01-22', '2016-01-22'),
(41, 'Australia (UTC+08:00)', 'AU ', '{"en":"Australia (UTC+08:00)"}', 'Australia/Perth', '1', '2016-01-22', '2016-01-22'),
(42, 'Australia (UTC+11:00)', 'AU ', '{"en":"Australia (UTC+11:00)"}', 'Australia/Sydney', '1', '2016-01-22', '2016-01-22'),
(43, 'Austria (UTC+01:00)', 'AT ', '{"en":"Austria (UTC+01:00)"}', 'Europe/Vienna', '1', '2016-01-22', '2016-01-22'),
(44, 'Azerbaijan (UTC+04:00)', 'AZ ', '{"en":"Azerbaijan (UTC+04:00)"}', 'Asia/Baku', '1', '2016-01-22', '2016-01-22'),
(45, 'Bahamas (UTC-05:00)', 'BS ', '{"en":"Bahamas (UTC-05:00)"}', 'America/Nassau', '1', '2016-01-22', '2016-01-22'),
(46, 'Bangladesh (UTC+06:00)', 'BD ', '{"en":"Bangladesh (UTC+06:00)"}', 'Asia/Dhaka', '1', '2016-01-22', '2016-01-22'),
(47, 'Barbados (UTC-04:00)', 'BB ', '{"en":"Barbados (UTC-04:00)"}', 'America/Barbados', '1', '2016-01-22', '2016-01-22'),
(48, 'Belarus (UTC+03:00)', 'BY ', '{"en":"Belarus (UTC+03:00)"}', 'Europe/Minsk', '1', '2016-01-22', '2016-01-22'),
(49, 'Belgium (UTC+01:00)', 'BE ', '{"en":"Belgium (UTC+01:00)"}', 'Europe/Brussels', '1', '2016-01-22', '2016-01-22'),
(50, 'Belize (UTC-06:00)', 'BZ ', '{"en":"Belize (UTC-06:00)"}', 'America/Belize', '1', '2016-01-22', '2016-01-22'),
(51, 'Bermuda (UTC-04:00)', 'BM ', '{"en":"Bermuda (UTC-04:00)"}', 'Atlantic/Bermuda', '1', '2016-01-22', '2016-01-22'),
(52, 'Bhutan (UTC+06:00)', 'BT ', '{"en":"Bhutan (UTC+06:00)"}', 'Asia/Thimphu', '1', '2016-01-22', '2016-01-22'),
(53, 'Bolivia (UTC-04:00)', 'BO ', '{"en":"Bolivia (UTC-04:00)"}', 'America/La_Paz', '1', '2016-01-22', '2016-01-22'),
(54, 'Bosnia & Herzegovina (UTC+01:00)', 'BA ', '{"en":"Bosnia & Herzegovina (UTC+01:00)"}', 'Europe/Sarajevo', '1', '2016-01-22', '2016-01-22'),
(55, 'Brazil (UTC-03:00)', 'BR ', '{"en":"Brazil (UTC-03:00)"}', 'America/Araguaina', '1', '2016-01-22', '2016-01-22'),
(56, 'Brazil (UTC-03:00)', 'BR ', '{"en":"Brazil (UTC-03:00)"}', 'America/Bahia', '1', '2016-01-22', '2016-01-22'),
(57, 'Brazil (UTC-03:00)', 'BR ', '{"en":"Brazil (UTC-03:00)"}', 'America/Belem', '1', '2016-01-22', '2016-01-22'),
(58, 'Brazil (UTC-04:00)', 'BR ', '{"en":"Brazil (UTC-04:00)"}', 'America/Boa_Vista', '1', '2016-01-22', '2016-01-22'),
(59, 'Brazil (UTC-03:00)', 'BR ', '{"en":"Brazil (UTC-03:00)"}', 'America/Campo_Grande', '1', '2016-01-22', '2016-01-22'),
(60, 'Brazil (UTC-03:00)', 'BR ', '{"en":"Brazil (UTC-03:00)"}', 'America/Cuiaba', '1', '2016-01-22', '2016-01-22'),
(61, 'Brazil (UTC-05:00)', 'BR ', '{"en":"Brazil (UTC-05:00)"}', 'America/Eirunepe', '1', '2016-01-22', '2016-01-22'),
(62, 'Brazil (UTC-03:00)', 'BR ', '{"en":"Brazil (UTC-03:00)"}', 'America/Fortaleza', '1', '2016-01-22', '2016-01-22'),
(63, 'Brazil (UTC-03:00)', 'BR ', '{"en":"Brazil (UTC-03:00)"}', 'America/Maceio', '1', '2016-01-22', '2016-01-22'),
(64, 'Brazil (UTC-04:00)', 'BR ', '{"en":"Brazil (UTC-04:00)"}', 'America/Manaus', '1', '2016-01-22', '2016-01-22'),
(65, 'Brazil (UTC-02:00)', 'BR ', '{"en":"Brazil (UTC-02:00)"}', 'America/Noronha', '1', '2016-01-22', '2016-01-22'),
(66, 'Brazil (UTC-04:00)', 'BR ', '{"en":"Brazil (UTC-04:00)"}', 'America/Porto_Velho', '1', '2016-01-22', '2016-01-22'),
(67, 'Brazil (UTC-03:00)', 'BR ', '{"en":"Brazil (UTC-03:00)"}', 'America/Recife', '1', '2016-01-22', '2016-01-22'),
(68, 'Brazil (UTC-05:00)', 'BR ', '{"en":"Brazil (UTC-05:00)"}', 'America/Rio_Branco', '1', '2016-01-22', '2016-01-22'),
(69, 'Brazil (UTC-03:00)', 'BR ', '{"en":"Brazil (UTC-03:00)"}', 'America/Santarem', '1', '2016-01-22', '2016-01-22'),
(70, 'Brazil (UTC-02:00)', 'BR ', '{"en":"Brazil (UTC-02:00)"}', 'America/Sao_Paulo', '1', '2016-01-22', '2016-01-22'),
(71, 'British Indian Ocean Territory (UTC+06:00)', 'IO ', '{"en":"British Indian Ocean Territory (UTC+06:00)"}', 'Indian/Chagos', '1', '2016-01-22', '2016-01-22'),
(72, 'Brunei (UTC+08:00)', 'BN ', '{"en":"Brunei (UTC+08:00)"}', 'Asia/Brunei', '1', '2016-01-22', '2016-01-22'),
(73, 'Bulgaria (UTC+02:00)', 'BG ', '{"en":"Bulgaria (UTC+02:00)"}', 'Europe/Sofia', '1', '2016-01-22', '2016-01-22'),
(74, 'Canada (UTC-05:00)', 'CA ', '{"en":"Canada (UTC-05:00)"}', 'America/Atikokan', '1', '2016-01-22', '2016-01-22'),
(75, 'Canada (UTC-04:00)', 'CA ', '{"en":"Canada (UTC-04:00)"}', 'America/Blanc-Sablon', '1', '2016-01-22', '2016-01-22'),
(76, 'Canada (UTC-07:00)', 'CA ', '{"en":"Canada (UTC-07:00)"}', 'America/Cambridge_Bay', '1', '2016-01-22', '2016-01-22'),
(77, 'Canada (UTC-07:00)', 'CA ', '{"en":"Canada (UTC-07:00)"}', 'America/Creston', '1', '2016-01-22', '2016-01-22'),
(78, 'Canada (UTC-08:00)', 'CA ', '{"en":"Canada (UTC-08:00)"}', 'America/Dawson', '1', '2016-01-22', '2016-01-22'),
(79, 'Canada (UTC-07:00)', 'CA ', '{"en":"Canada (UTC-07:00)"}', 'America/Dawson_Creek', '1', '2016-01-22', '2016-01-22'),
(80, 'Canada (UTC-07:00)', 'CA ', '{"en":"Canada (UTC-07:00)"}', 'America/Edmonton', '1', '2016-01-22', '2016-01-22'),
(81, 'Canada (UTC-07:00)', 'CA ', '{"en":"Canada (UTC-07:00)"}', 'America/Fort_Nelson', '1', '2016-01-22', '2016-01-22'),
(82, 'Canada (UTC-04:00)', 'CA ', '{"en":"Canada (UTC-04:00)"}', 'America/Glace_Bay', '1', '2016-01-22', '2016-01-22'),
(83, 'Canada (UTC-04:00)', 'CA ', '{"en":"Canada (UTC-04:00)"}', 'America/Goose_Bay', '1', '2016-01-22', '2016-01-22'),
(84, 'Canada (UTC-04:00)', 'CA ', '{"en":"Canada (UTC-04:00)"}', 'America/Halifax', '1', '2016-01-22', '2016-01-22'),
(85, 'Canada (UTC-07:00)', 'CA ', '{"en":"Canada (UTC-07:00)"}', 'America/Inuvik', '1', '2016-01-22', '2016-01-22'),
(86, 'Canada (UTC-05:00)', 'CA ', '{"en":"Canada (UTC-05:00)"}', 'America/Iqaluit', '1', '2016-01-22', '2016-01-22'),
(87, 'Canada (UTC-04:00)', 'CA ', '{"en":"Canada (UTC-04:00)"}', 'America/Moncton', '1', '2016-01-22', '2016-01-22'),
(88, 'Canada (UTC-05:00)', 'CA ', '{"en":"Canada (UTC-05:00)"}', 'America/Nipigon', '1', '2016-01-22', '2016-01-22'),
(89, 'Canada (UTC-05:00)', 'CA ', '{"en":"Canada (UTC-05:00)"}', 'America/Pangnirtung', '1', '2016-01-22', '2016-01-22'),
(90, 'Canada (UTC-06:00)', 'CA ', '{"en":"Canada (UTC-06:00)"}', 'America/Rainy_River', '1', '2016-01-22', '2016-01-22'),
(91, 'Canada (UTC-06:00)', 'CA ', '{"en":"Canada (UTC-06:00)"}', 'America/Rankin_Inlet', '1', '2016-01-22', '2016-01-22'),
(92, 'Canada (UTC-06:00)', 'CA ', '{"en":"Canada (UTC-06:00)"}', 'America/Regina', '1', '2016-01-22', '2016-01-22'),
(93, 'Canada (UTC-06:00)', 'CA ', '{"en":"Canada (UTC-06:00)"}', 'America/Resolute', '1', '2016-01-22', '2016-01-22'),
(94, 'Canada (UTC-03:30)', 'CA ', '{"en":"Canada (UTC-03:30)"}', 'America/St_Johns', '1', '2016-01-22', '2016-01-22'),
(95, 'Canada (UTC-06:00)', 'CA ', '{"en":"Canada (UTC-06:00)"}', 'America/Swift_Current', '1', '2016-01-22', '2016-01-22'),
(96, 'Canada (UTC-05:00)', 'CA ', '{"en":"Canada (UTC-05:00)"}', 'America/Thunder_Bay', '1', '2016-01-22', '2016-01-22'),
(97, 'Canada (UTC-05:00)', 'CA ', '{"en":"Canada (UTC-05:00)"}', 'America/Toronto', '1', '2016-01-22', '2016-01-22'),
(98, 'Canada (UTC-08:00)', 'CA ', '{"en":"Canada (UTC-08:00)"}', 'America/Vancouver', '1', '2016-01-22', '2016-01-22'),
(99, 'Canada (UTC-08:00)', 'CA ', '{"en":"Canada (UTC-08:00)"}', 'America/Whitehorse', '1', '2016-01-22', '2016-01-22'),
(100, 'Canada (UTC-06:00)', 'CA ', '{"en":"Canada (UTC-06:00)"}', 'America/Winnipeg', '1', '2016-01-22', '2016-01-22'),
(101, 'Canada (UTC-07:00)', 'CA ', '{"en":"Canada (UTC-07:00)"}', 'America/Yellowknife', '1', '2016-01-22', '2016-01-22'),
(102, 'Cape Verde (UTC-01:00)', 'CV ', '{"en":"Cape Verde (UTC-01:00)"}', 'Atlantic/Cape_Verde', '1', '2016-01-22', '2016-01-22'),
(103, 'Cayman Islands (UTC-05:00)', 'KY ', '{"en":"Cayman Islands (UTC-05:00)"}', 'America/Cayman', '1', '2016-01-22', '2016-01-22'),
(104, 'Chad (UTC+01:00)', 'TD ', '{"en":"Chad (UTC+01:00)"}', 'Africa/Ndjamena', '1', '2016-01-22', '2016-01-22'),
(105, 'Chile (UTC-03:00)', 'CL ', '{"en":"Chile (UTC-03:00)"}', 'America/Santiago', '1', '2016-01-22', '2016-01-22'),
(106, 'Chile (UTC-05:00)', 'CL ', '{"en":"Chile (UTC-05:00)"}', 'Pacific/Easter', '1', '2016-01-22', '2016-01-22'),
(107, 'China (UTC+08:00)', 'CN ', '{"en":"China (UTC+08:00)"}', 'Asia/Shanghai', '1', '2016-01-22', '2016-01-22'),
(108, 'China (UTC+06:00)', 'CN ', '{"en":"China (UTC+06:00)"}', 'Asia/Urumqi', '1', '2016-01-22', '2016-01-22'),
(109, 'Christmas Island (UTC+07:00)', 'CX ', '{"en":"Christmas Island (UTC+07:00)"}', 'Indian/Christmas', '1', '2016-01-22', '2016-01-22'),
(110, 'Cocos (Keeling) Islands (UTC+06:30)', 'CC ', '{"en":"Cocos (Keeling) Islands (UTC+06:30)"}', 'Indian/Cocos', '1', '2016-01-22', '2016-01-22'),
(111, 'Colombia (UTC-05:00)', 'CO ', '{"en":"Colombia (UTC-05:00)"}', 'America/Bogota', '1', '2016-01-22', '2016-01-22'),
(112, 'Cook Islands (UTC-10:00)', 'CK ', '{"en":"Cook Islands (UTC-10:00)"}', 'Pacific/Rarotonga', '1', '2016-01-22', '2016-01-22'),
(113, 'Costa Rica (UTC-06:00)', 'CR ', '{"en":"Costa Rica (UTC-06:00)"}', 'America/Costa_Rica', '1', '2016-01-22', '2016-01-22'),
(114, 'Côte d’Ivoire (UTC+00:00)', 'CI ', '{"en":"Cu00f4te du2019Ivoire (UTC+00:00)"}', 'Africa/Abidjan', '1', '2016-01-22', '2016-01-22'),
(115, 'Croatia (UTC+01:00)', 'HR ', '{"en":"Croatia (UTC+01:00)"}', 'Europe/Zagreb', '1', '2016-01-22', '2016-01-22'),
(116, 'Cuba (UTC-05:00)', 'CU ', '{"en":"Cuba (UTC-05:00)"}', 'America/Havana', '1', '2016-01-22', '2016-01-22'),
(117, 'Curaçao (UTC-04:00)', 'CW ', '{"en":"Curau00e7ao (UTC-04:00)"}', 'America/Curacao', '1', '2016-01-22', '2016-01-22'),
(118, 'Cyprus (UTC+02:00)', 'CY ', '{"en":"Cyprus (UTC+02:00)"}', 'Asia/Nicosia', '1', '2016-01-22', '2016-01-22'),
(119, 'Czech Republic (UTC+01:00)', 'CZ ', '{"en":"Czech Republic (UTC+01:00)"}', 'Europe/Prague', '1', '2016-01-22', '2016-01-22'),
(120, 'Denmark (UTC+01:00)', 'DK ', '{"en":"Denmark (UTC+01:00)"}', 'Europe/Copenhagen', '1', '2016-01-22', '2016-01-22'),
(121, 'Dominican Republic (UTC-04:00)', 'DO ', '{"en":"Dominican Republic (UTC-04:00)"}', 'America/Santo_Domingo', '1', '2016-01-22', '2016-01-22'),
(122, 'Ecuador (UTC-05:00)', 'EC ', '{"en":"Ecuador (UTC-05:00)"}', 'America/Guayaquil', '1', '2016-01-22', '2016-01-22'),
(123, 'Ecuador (UTC-06:00)', 'EC ', '{"en":"Ecuador (UTC-06:00)"}', 'Pacific/Galapagos', '1', '2016-01-22', '2016-01-22'),
(124, 'Egypt (UTC+02:00)', 'EG ', '{"en":"Egypt (UTC+02:00)"}', 'Africa/Cairo', '1', '2016-01-22', '2016-01-22'),
(125, 'El Salvador (UTC-06:00)', 'SV ', '{"en":"El Salvador (UTC-06:00)"}', 'America/El_Salvador', '1', '2016-01-22', '2016-01-22'),
(126, 'Estonia (UTC+02:00)', 'EE ', '{"en":"Estonia (UTC+02:00)"}', 'Europe/Tallinn', '1', '2016-01-22', '2016-01-22'),
(127, 'Falkland Islands (Islas Malvinas) (UTC-03:00)', 'FK ', '{"en":"Falkland Islands (Islas Malvinas) (UTC-03:00)"}', 'Atlantic/Stanley', '1', '2016-01-22', '2016-01-22'),
(128, 'Faroe Islands (UTC+00:00)', 'FO ', '{"en":"Faroe Islands (UTC+00:00)"}', 'Atlantic/Faroe', '1', '2016-01-22', '2016-01-22'),
(129, 'Fiji (UTC+12:00)', 'FJ ', '{"en":"Fiji (UTC+12:00)"}', 'Pacific/Fiji', '1', '2016-01-22', '2016-01-22'),
(130, 'Finland (UTC+02:00)', 'FI ', '{"en":"Finland (UTC+02:00)"}', 'Europe/Helsinki', '1', '2016-01-22', '2016-01-22'),
(131, 'France (UTC+01:00)', 'FR ', '{"en":"France (UTC+01:00)"}', 'Europe/Paris', '1', '2016-01-22', '2016-01-22'),
(132, 'French Guiana (UTC-03:00)', 'GF ', '{"en":"French Guiana (UTC-03:00)"}', 'America/Cayenne', '1', '2016-01-22', '2016-01-22'),
(133, 'French Polynesia (UTC-09:00)', 'PF ', '{"en":"French Polynesia (UTC-09:00)"}', 'Pacific/Gambier', '1', '2016-01-22', '2016-01-22'),
(134, 'French Polynesia (UTC-09:30)', 'PF ', '{"en":"French Polynesia (UTC-09:30)"}', 'Pacific/Marquesas', '1', '2016-01-22', '2016-01-22'),
(135, 'French Polynesia (UTC-10:00)', 'PF ', '{"en":"French Polynesia (UTC-10:00)"}', 'Pacific/Tahiti', '1', '2016-01-22', '2016-01-22'),
(136, 'French Southern Territories (UTC+05:00)', 'TF ', '{"en":"French Southern Territories (UTC+05:00)"}', 'Indian/Kerguelen', '1', '2016-01-22', '2016-01-22'),
(137, 'Georgia (UTC+04:00)', 'GE ', '{"en":"Georgia (UTC+04:00)"}', 'Asia/Tbilisi', '1', '2016-01-22', '2016-01-22'),
(138, 'Germany (UTC+01:00)', 'DE ', '{"en":"Germany (UTC+01:00)"}', 'Europe/Berlin', '1', '2016-01-22', '2016-01-22'),
(139, 'Germany (UTC+01:00)', 'DE ', '{"en":"Germany (UTC+01:00)"}', 'Europe/Busingen', '1', '2016-01-22', '2016-01-22'),
(140, 'Ghana (UTC+00:00)', 'GH ', '{"en":"Ghana (UTC+00:00)"}', 'Africa/Accra', '1', '2016-01-22', '2016-01-22'),
(141, 'Gibraltar (UTC+01:00)', 'GI ', '{"en":"Gibraltar (UTC+01:00)"}', 'Europe/Gibraltar', '1', '2016-01-22', '2016-01-22'),
(142, 'Greece (UTC+02:00)', 'GR ', '{"en":"Greece (UTC+02:00)"}', 'Europe/Athens', '1', '2016-01-22', '2016-01-22'),
(143, 'Greenland (UTC+00:00)', 'GL ', '{"en":"Greenland (UTC+00:00)"}', 'America/Danmarkshavn', '1', '2016-01-22', '2016-01-22'),
(144, 'Greenland (UTC-03:00)', 'GL ', '{"en":"Greenland (UTC-03:00)"}', 'America/Godthab', '1', '2016-01-22', '2016-01-22'),
(145, 'Greenland (UTC-01:00)', 'GL ', '{"en":"Greenland (UTC-01:00)"}', 'America/Scoresbysund', '1', '2016-01-22', '2016-01-22'),
(146, 'Greenland (UTC-04:00)', 'GL ', '{"en":"Greenland (UTC-04:00)"}', 'America/Thule', '1', '2016-01-22', '2016-01-22'),
(147, 'Guam (UTC+10:00)', 'GU ', '{"en":"Guam (UTC+10:00)"}', 'Pacific/Guam', '1', '2016-01-22', '2016-01-22'),
(148, 'Guatemala (UTC-06:00)', 'GT ', '{"en":"Guatemala (UTC-06:00)"}', 'America/Guatemala', '1', '2016-01-22', '2016-01-22'),
(149, 'Guinea-Bissau (UTC+00:00)', 'GW ', '{"en":"Guinea-Bissau (UTC+00:00)"}', 'Africa/Bissau', '1', '2016-01-22', '2016-01-22'),
(150, 'Guyana (UTC-04:00)', 'GY ', '{"en":"Guyana (UTC-04:00)"}', 'America/Guyana', '1', '2016-01-22', '2016-01-22'),
(151, 'Haiti (UTC-05:00)', 'HT ', '{"en":"Haiti (UTC-05:00)"}', 'America/Port-au-Prince', '1', '2016-01-22', '2016-01-22'),
(152, 'Honduras (UTC-06:00)', 'HN ', '{"en":"Honduras (UTC-06:00)"}', 'America/Tegucigalpa', '1', '2016-01-22', '2016-01-22'),
(153, 'Hong Kong (UTC+08:00)', 'HK ', '{"en":"Hong Kong (UTC+08:00)"}', 'Asia/Hong_Kong', '1', '2016-01-22', '2016-01-22'),
(154, 'Hungary (UTC+01:00)', 'HU ', '{"en":"Hungary (UTC+01:00)"}', 'Europe/Budapest', '1', '2016-01-22', '2016-01-22'),
(155, 'Iceland (UTC+00:00)', 'IS ', '{"en":"Iceland (UTC+00:00)"}', 'Atlantic/Reykjavik', '1', '2016-01-22', '2016-01-22'),
(156, 'India (UTC+05:30)', 'IN ', '{"en":"India (UTC+05:30)"}', 'Asia/Kolkata', '1', '2016-01-22', '2016-01-22'),
(157, 'Indonesia (UTC+07:00)', 'ID ', '{"en":"Indonesia (UTC+07:00)"}', 'Asia/Jakarta', '1', '2016-01-22', '2016-01-22'),
(158, 'Indonesia (UTC+09:00)', 'ID ', '{"en":"Indonesia (UTC+09:00)"}', 'Asia/Jayapura', '1', '2016-01-22', '2016-01-22'),
(159, 'Indonesia (UTC+08:00)', 'ID ', '{"en":"Indonesia (UTC+08:00)"}', 'Asia/Makassar', '1', '2016-01-22', '2016-01-22'),
(160, 'Indonesia (UTC+07:00)', 'ID ', '{"en":"Indonesia (UTC+07:00)"}', 'Asia/Pontianak', '1', '2016-01-22', '2016-01-22'),
(161, 'Iran (UTC+03:30)', 'IR ', '{"en":"Iran (UTC+03:30)"}', 'Asia/Tehran', '1', '2016-01-22', '2016-01-22'),
(162, 'Iraq (UTC+03:00)', 'IQ ', '{"en":"Iraq (UTC+03:00)"}', 'Asia/Baghdad', '1', '2016-01-22', '2016-01-22'),
(163, 'Ireland (UTC+00:00)', 'IE ', '{"en":"Ireland (UTC+00:00)"}', 'Europe/Dublin', '1', '2016-01-22', '2016-01-22'),
(164, 'Israel (UTC+02:00)', 'IL ', '{"en":"Israel (UTC+02:00)"}', 'Asia/Jerusalem', '1', '2016-01-22', '2016-01-22'),
(165, 'Italy (UTC+01:00)', 'IT ', '{"en":"Italy (UTC+01:00)"}', 'Europe/Rome', '1', '2016-01-22', '2016-01-22'),
(166, 'Jamaica (UTC-05:00)', 'JM ', '{"en":"Jamaica (UTC-05:00)"}', 'America/Jamaica', '1', '2016-01-22', '2016-01-22'),
(167, 'Japan (UTC+09:00)', 'JP ', '{"en":"Japan (UTC+09:00)"}', 'Asia/Tokyo', '1', '2016-01-22', '2016-01-22'),
(168, 'Jordan (UTC+02:00)', 'JO ', '{"en":"Jordan (UTC+02:00)"}', 'Asia/Amman', '1', '2016-01-22', '2016-01-22'),
(169, 'Kazakhstan (UTC+06:00)', 'KZ ', '{"en":"Kazakhstan (UTC+06:00)"}', 'Asia/Almaty', '1', '2016-01-22', '2016-01-22'),
(170, 'Kazakhstan (UTC+05:00)', 'KZ ', '{"en":"Kazakhstan (UTC+05:00)"}', 'Asia/Aqtau', '1', '2016-01-22', '2016-01-22'),
(171, 'Kazakhstan (UTC+05:00)', 'KZ ', '{"en":"Kazakhstan (UTC+05:00)"}', 'Asia/Aqtobe', '1', '2016-01-22', '2016-01-22'),
(172, 'Kazakhstan (UTC+05:00)', 'KZ ', '{"en":"Kazakhstan (UTC+05:00)"}', 'Asia/Oral', '1', '2016-01-22', '2016-01-22'),
(173, 'Kazakhstan (UTC+06:00)', 'KZ ', '{"en":"Kazakhstan (UTC+06:00)"}', 'Asia/Qyzylorda', '1', '2016-01-22', '2016-01-22'),
(174, 'Kenya (UTC+03:00)', 'KE ', '{"en":"Kenya (UTC+03:00)"}', 'Africa/Nairobi', '1', '2016-01-22', '2016-01-22'),
(175, 'Kiribati (UTC+13:00)', 'KI ', '{"en":"Kiribati (UTC+13:00)"}', 'Pacific/Enderbury', '1', '2016-01-22', '2016-01-22'),
(176, 'Kiribati (UTC+14:00)', 'KI ', '{"en":"Kiribati (UTC+14:00)"}', 'Pacific/Kiritimati', '1', '2016-01-22', '2016-01-22'),
(177, 'Kiribati (UTC+12:00)', 'KI ', '{"en":"Kiribati (UTC+12:00)"}', 'Pacific/Tarawa', '1', '2016-01-22', '2016-01-22'),
(178, 'Kyrgyzstan (UTC+06:00)', 'KG ', '{"en":"Kyrgyzstan (UTC+06:00)"}', 'Asia/Bishkek', '1', '2016-01-22', '2016-01-22'),
(179, 'Latvia (UTC+02:00)', 'LV ', '{"en":"Latvia (UTC+02:00)"}', 'Europe/Riga', '1', '2016-01-22', '2016-01-22'),
(180, 'Lebanon (UTC+02:00)', 'LB ', '{"en":"Lebanon (UTC+02:00)"}', 'Asia/Beirut', '1', '2016-01-22', '2016-01-22'),
(181, 'Liberia (UTC+00:00)', 'LR ', '{"en":"Liberia (UTC+00:00)"}', 'Africa/Monrovia', '1', '2016-01-22', '2016-01-22'),
(182, 'Libya (UTC+02:00)', 'LY ', '{"en":"Libya (UTC+02:00)"}', 'Africa/Tripoli', '1', '2016-01-22', '2016-01-22'),
(183, 'Lithuania (UTC+02:00)', 'LT ', '{"en":"Lithuania (UTC+02:00)"}', 'Europe/Vilnius', '1', '2016-01-22', '2016-01-22'),
(184, 'Luxembourg (UTC+01:00)', 'LU ', '{"en":"Luxembourg (UTC+01:00)"}', 'Europe/Luxembourg', '1', '2016-01-22', '2016-01-22'),
(185, 'Macau (UTC+08:00)', 'MO ', '{"en":"Macau (UTC+08:00)"}', 'Asia/Macau', '1', '2016-01-22', '2016-01-22'),
(186, 'Macedonia (FYROM) (UTC+01:00)', 'MK ', '{"en":"Macedonia (FYROM) (UTC+01:00)"}', 'Europe/Skopje', '1', '2016-01-22', '2016-01-22'),
(187, 'Malaysia (UTC+08:00)', 'MY ', '{"en":"Malaysia (UTC+08:00)"}', 'Asia/Kuala_Lumpur', '1', '2016-01-22', '2016-01-22'),
(188, 'Malaysia (UTC+08:00)', 'MY ', '{"en":"Malaysia (UTC+08:00)"}', 'Asia/Kuching', '1', '2016-01-22', '2016-01-22'),
(189, 'Maldives (UTC+05:00)', 'MV ', '{"en":"Maldives (UTC+05:00)"}', 'Indian/Maldives', '1', '2016-01-22', '2016-01-22'),
(190, 'Malta (UTC+01:00)', 'MT ', '{"en":"Malta (UTC+01:00)"}', 'Europe/Malta', '1', '2016-01-22', '2016-01-22'),
(191, 'Marshall Islands (UTC+12:00)', 'MH ', '{"en":"Marshall Islands (UTC+12:00)"}', 'Pacific/Kwajalein', '1', '2016-01-22', '2016-01-22'),
(192, 'Marshall Islands (UTC+12:00)', 'MH ', '{"en":"Marshall Islands (UTC+12:00)"}', 'Pacific/Majuro', '1', '2016-01-22', '2016-01-22'),
(193, 'Martinique (UTC-04:00)', 'MQ ', '{"en":"Martinique (UTC-04:00)"}', 'America/Martinique', '1', '2016-01-22', '2016-01-22'),
(194, 'Mauritius (UTC+04:00)', 'MU ', '{"en":"Mauritius (UTC+04:00)"}', 'Indian/Mauritius', '1', '2016-01-22', '2016-01-22'),
(195, 'Mexico (UTC-06:00)', 'MX ', '{"en":"Mexico (UTC-06:00)"}', 'America/Bahia_Banderas', '1', '2016-01-22', '2016-01-22'),
(196, 'Mexico (UTC-05:00)', 'MX ', '{"en":"Mexico (UTC-05:00)"}', 'America/Cancun', '1', '2016-01-22', '2016-01-22'),
(197, 'Mexico (UTC-07:00)', 'MX ', '{"en":"Mexico (UTC-07:00)"}', 'America/Chihuahua', '1', '2016-01-22', '2016-01-22'),
(198, 'Mexico (UTC-07:00)', 'MX ', '{"en":"Mexico (UTC-07:00)"}', 'America/Hermosillo', '1', '2016-01-22', '2016-01-22'),
(199, 'Mexico (UTC-06:00)', 'MX ', '{"en":"Mexico (UTC-06:00)"}', 'America/Matamoros', '1', '2016-01-22', '2016-01-22'),
(200, 'Mexico (UTC-07:00)', 'MX ', '{"en":"Mexico (UTC-07:00)"}', 'America/Mazatlan', '1', '2016-01-22', '2016-01-22'),
(201, 'Mexico (UTC-06:00)', 'MX ', '{"en":"Mexico (UTC-06:00)"}', 'America/Merida', '1', '2016-01-22', '2016-01-22'),
(202, 'Mexico (UTC-06:00)', 'MX ', '{"en":"Mexico (UTC-06:00)"}', 'America/Mexico_City', '1', '2016-01-22', '2016-01-22'),
(203, 'Mexico (UTC-06:00)', 'MX ', '{"en":"Mexico (UTC-06:00)"}', 'America/Monterrey', '1', '2016-01-22', '2016-01-22'),
(204, 'Mexico (UTC-07:00)', 'MX ', '{"en":"Mexico (UTC-07:00)"}', 'America/Ojinaga', '1', '2016-01-22', '2016-01-22'),
(205, 'Mexico (UTC-08:00)', 'MX ', '{"en":"Mexico (UTC-08:00)"}', 'America/Santa_Isabel', '1', '2016-01-22', '2016-01-22'),
(206, 'Mexico (UTC-08:00)', 'MX ', '{"en":"Mexico (UTC-08:00)"}', 'America/Tijuana', '1', '2016-01-22', '2016-01-22'),
(207, 'Micronesia (UTC+10:00)', 'FM ', '{"en":"Micronesia (UTC+10:00)"}', 'Pacific/Chuuk', '1', '2016-01-22', '2016-01-22'),
(208, 'Micronesia (UTC+11:00)', 'FM ', '{"en":"Micronesia (UTC+11:00)"}', 'Pacific/Kosrae', '1', '2016-01-22', '2016-01-22'),
(209, 'Micronesia (UTC+11:00)', 'FM ', '{"en":"Micronesia (UTC+11:00)"}', 'Pacific/Pohnpei', '1', '2016-01-22', '2016-01-22'),
(210, 'Moldova (UTC+02:00)', 'MD ', '{"en":"Moldova (UTC+02:00)"}', 'Europe/Chisinau', '1', '2016-01-22', '2016-01-22'),
(211, 'Monaco (UTC+01:00)', 'MC ', '{"en":"Monaco (UTC+01:00)"}', 'Europe/Monaco', '1', '2016-01-22', '2016-01-22'),
(212, 'Mongolia (UTC+08:00)', 'MN ', '{"en":"Mongolia (UTC+08:00)"}', 'Asia/Choibalsan', '1', '2016-01-22', '2016-01-22'),
(213, 'Mongolia (UTC+07:00)', 'MN ', '{"en":"Mongolia (UTC+07:00)"}', 'Asia/Hovd', '1', '2016-01-22', '2016-01-22'),
(214, 'Mongolia (UTC+08:00)', 'MN ', '{"en":"Mongolia (UTC+08:00)"}', 'Asia/Ulaanbaatar', '1', '2016-01-22', '2016-01-22'),
(215, 'Morocco (UTC+00:00)', 'MA ', '{"en":"Morocco (UTC+00:00)"}', 'Africa/Casablanca', '1', '2016-01-22', '2016-01-22'),
(216, 'Mozambique (UTC+02:00)', 'MZ ', '{"en":"Mozambique (UTC+02:00)"}', 'Africa/Maputo', '1', '2016-01-22', '2016-01-22'),
(217, 'Myanmar (Burma) (UTC+06:30)', 'MM ', '{"en":"Myanmar (Burma) (UTC+06:30)"}', 'Asia/Rangoon', '1', '2016-01-22', '2016-01-22'),
(218, 'Namibia (UTC+02:00)', 'NA ', '{"en":"Namibia (UTC+02:00)"}', 'Africa/Windhoek', '1', '2016-01-22', '2016-01-22'),
(219, 'Nauru (UTC+12:00)', 'NR ', '{"en":"Nauru (UTC+12:00)"}', 'Pacific/Nauru', '1', '2016-01-22', '2016-01-22'),
(220, 'Nepal (UTC+05:45)', 'NP ', '{"en":"Nepal (UTC+05:45)"}', 'Asia/Kathmandu', '1', '2016-01-22', '2016-01-22'),
(221, 'Netherlands (UTC+01:00)', 'NL ', '{"en":"Netherlands (UTC+01:00)"}', 'Europe/Amsterdam', '1', '2016-01-22', '2016-01-22'),
(222, 'New Caledonia (UTC+11:00)', 'NC ', '{"en":"New Caledonia (UTC+11:00)"}', 'Pacific/Noumea', '1', '2016-01-22', '2016-01-22'),
(223, 'New Zealand (UTC+13:00)', 'NZ ', '{"en":"New Zealand (UTC+13:00)"}', 'Pacific/Auckland', '1', '2016-01-22', '2016-01-22'),
(224, 'New Zealand (UTC+13:45)', 'NZ ', '{"en":"New Zealand (UTC+13:45)"}', 'Pacific/Chatham', '1', '2016-01-22', '2016-01-22'),
(225, 'Nicaragua (UTC-06:00)', 'NI ', '{"en":"Nicaragua (UTC-06:00)"}', 'America/Managua', '1', '2016-01-22', '2016-01-22'),
(226, 'Nigeria (UTC+01:00)', 'NG ', '{"en":"Nigeria (UTC+01:00)"}', 'Africa/Lagos', '1', '2016-01-22', '2016-01-22'),
(227, 'Niue (UTC-11:00)', 'NU ', '{"en":"Niue (UTC-11:00)"}', 'Pacific/Niue', '1', '2016-01-22', '2016-01-22'),
(228, 'Norfolk Island (UTC+11:00)', 'NF ', '{"en":"Norfolk Island (UTC+11:00)"}', 'Pacific/Norfolk', '1', '2016-01-22', '2016-01-22'),
(229, 'North Korea (UTC+08:30)', 'KP ', '{"en":"North Korea (UTC+08:30)"}', 'Asia/Pyongyang', '1', '2016-01-22', '2016-01-22'),
(230, 'Northern Mariana Islands (UTC+10:00)', 'MP ', '{"en":"Northern Mariana Islands (UTC+10:00)"}', 'Pacific/Saipan', '1', '2016-01-22', '2016-01-22'),
(231, 'Norway (UTC+01:00)', 'NO ', '{"en":"Norway (UTC+01:00)"}', 'Europe/Oslo', '1', '2016-01-22', '2016-01-22'),
(232, 'Pakistan (UTC+05:00)', 'PK ', '{"en":"Pakistan (UTC+05:00)"}', 'Asia/Karachi', '1', '2016-01-22', '2016-01-22'),
(233, 'Palau (UTC+09:00)', 'PW ', '{"en":"Palau (UTC+09:00)"}', 'Pacific/Palau', '1', '2016-01-22', '2016-01-22'),
(234, 'Palestine (UTC+02:00)', 'PS ', '{"en":"Palestine (UTC+02:00)"}', 'Asia/Gaza', '1', '2016-01-22', '2016-01-22'),
(235, 'Palestine (UTC+02:00)', 'PS ', '{"en":"Palestine (UTC+02:00)"}', 'Asia/Hebron', '1', '2016-01-22', '2016-01-22'),
(236, 'Panama (UTC-05:00)', 'PA ', '{"en":"Panama (UTC-05:00)"}', 'America/Panama', '1', '2016-01-22', '2016-01-22'),
(237, 'Papua New Guinea (UTC+11:00)', 'PG ', '{"en":"Papua New Guinea (UTC+11:00)"}', 'Pacific/Bougainville', '1', '2016-01-22', '2016-01-22'),
(238, 'Papua New Guinea (UTC+10:00)', 'PG ', '{"en":"Papua New Guinea (UTC+10:00)"}', 'Pacific/Port_Moresby', '1', '2016-01-22', '2016-01-22'),
(239, 'Paraguay (UTC-03:00)', 'PY ', '{"en":"Paraguay (UTC-03:00)"}', 'America/Asuncion', '1', '2016-01-22', '2016-01-22'),
(240, 'Peru (UTC-05:00)', 'PE ', '{"en":"Peru (UTC-05:00)"}', 'America/Lima', '1', '2016-01-22', '2016-01-22'),
(241, 'Philippines (UTC+08:00)', 'PH ', '{"en":"Philippines (UTC+08:00)"}', 'Asia/Manila', '1', '2016-01-22', '2016-01-22'),
(242, 'Pitcairn Islands (UTC-08:00)', 'PN ', '{"en":"Pitcairn Islands (UTC-08:00)"}', 'Pacific/Pitcairn', '1', '2016-01-22', '2016-01-22'),
(243, 'Poland (UTC+01:00)', 'PL ', '{"en":"Poland (UTC+01:00)"}', 'Europe/Warsaw', '1', '2016-01-22', '2016-01-22'),
(244, 'Portugal (UTC-01:00)', 'PT ', '{"en":"Portugal (UTC-01:00)"}', 'Atlantic/Azores', '1', '2016-01-22', '2016-01-22'),
(245, 'Portugal (UTC+00:00)', 'PT ', '{"en":"Portugal (UTC+00:00)"}', 'Atlantic/Madeira', '1', '2016-01-22', '2016-01-22'),
(246, 'Portugal (UTC+00:00)', 'PT ', '{"en":"Portugal (UTC+00:00)"}', 'Europe/Lisbon', '1', '2016-01-22', '2016-01-22'),
(247, 'Puerto Rico (UTC-04:00)', 'PR ', '{"en":"Puerto Rico (UTC-04:00)"}', 'America/Puerto_Rico', '1', '2016-01-22', '2016-01-22'),
(248, 'Qatar (UTC+03:00)', 'QA ', '{"en":"Qatar (UTC+03:00)"}', 'Asia/Qatar', '1', '2016-01-22', '2016-01-22'),
(249, 'Réunion (UTC+04:00)', 'RE ', '{"en":"Ru00e9union (UTC+04:00)"}', 'Indian/Reunion', '1', '2016-01-22', '2016-01-22'),
(250, 'Romania (UTC+02:00)', 'RO ', '{"en":"Romania (UTC+02:00)"}', 'Europe/Bucharest', '1', '2016-01-22', '2016-01-22'),
(251, 'Russia (UTC+12:00)', 'RU ', '{"en":"Russia (UTC+12:00)"}', 'Asia/Anadyr', '1', '2016-01-22', '2016-01-22'),
(252, 'Russia (UTC+08:00)', 'RU ', '{"en":"Russia (UTC+08:00)"}', 'Asia/Chita', '1', '2016-01-22', '2016-01-22'),
(253, 'Russia (UTC+08:00)', 'RU ', '{"en":"Russia (UTC+08:00)"}', 'Asia/Irkutsk', '1', '2016-01-22', '2016-01-22'),
(254, 'Russia (UTC+12:00)', 'RU ', '{"en":"Russia (UTC+12:00)"}', 'Asia/Kamchatka', '1', '2016-01-22', '2016-01-22'),
(255, 'Russia (UTC+09:00)', 'RU ', '{"en":"Russia (UTC+09:00)"}', 'Asia/Khandyga', '1', '2016-01-22', '2016-01-22'),
(256, 'Russia (UTC+07:00)', 'RU ', '{"en":"Russia (UTC+07:00)"}', 'Asia/Krasnoyarsk', '1', '2016-01-22', '2016-01-22'),
(257, 'Russia (UTC+10:00)', 'RU ', '{"en":"Russia (UTC+10:00)"}', 'Asia/Magadan', '1', '2016-01-22', '2016-01-22'),
(258, 'Russia (UTC+07:00)', 'RU ', '{"en":"Russia (UTC+07:00)"}', 'Asia/Novokuznetsk', '1', '2016-01-22', '2016-01-22'),
(259, 'Russia (UTC+06:00)', 'RU ', '{"en":"Russia (UTC+06:00)"}', 'Asia/Novosibirsk', '1', '2016-01-22', '2016-01-22'),
(260, 'Russia (UTC+06:00)', 'RU ', '{"en":"Russia (UTC+06:00)"}', 'Asia/Omsk', '1', '2016-01-22', '2016-01-22'),
(261, 'Russia (UTC+10:00)', 'RU ', '{"en":"Russia (UTC+10:00)"}', 'Asia/Sakhalin', '1', '2016-01-22', '2016-01-22'),
(262, 'Russia (UTC+11:00)', 'RU ', '{"en":"Russia (UTC+11:00)"}', 'Asia/Srednekolymsk', '1', '2016-01-22', '2016-01-22'),
(263, 'Russia (UTC+10:00)', 'RU ', '{"en":"Russia (UTC+10:00)"}', 'Asia/Ust-Nera', '1', '2016-01-22', '2016-01-22'),
(264, 'Russia (UTC+10:00)', 'RU ', '{"en":"Russia (UTC+10:00)"}', 'Asia/Vladivostok', '1', '2016-01-22', '2016-01-22'),
(265, 'Russia (UTC+09:00)', 'RU ', '{"en":"Russia (UTC+09:00)"}', 'Asia/Yakutsk', '1', '2016-01-22', '2016-01-22'),
(266, 'Russia (UTC+05:00)', 'RU ', '{"en":"Russia (UTC+05:00)"}', 'Asia/Yekaterinburg', '1', '2016-01-22', '2016-01-22'),
(267, 'Russia (UTC+02:00)', 'RU ', '{"en":"Russia (UTC+02:00)"}', 'Europe/Kaliningrad', '1', '2016-01-22', '2016-01-22'),
(268, 'Russia (UTC+03:00)', 'RU ', '{"en":"Russia (UTC+03:00)"}', 'Europe/Moscow', '1', '2016-01-22', '2016-01-22'),
(269, 'Russia (UTC+04:00)', 'RU ', '{"en":"Russia (UTC+04:00)"}', 'Europe/Samara', '1', '2016-01-22', '2016-01-22'),
(270, 'Russia (UTC+03:00)', 'RU ', '{"en":"Russia (UTC+03:00)"}', 'Europe/Simferopol', '1', '2016-01-22', '2016-01-22'),
(271, 'Russia (UTC+03:00)', 'RU ', '{"en":"Russia (UTC+03:00)"}', 'Europe/Volgograd', '1', '2016-01-22', '2016-01-22'),
(272, 'Samoa (UTC+14:00)', 'WS ', '{"en":"Samoa (UTC+14:00)"}', 'Pacific/Apia', '1', '2016-01-22', '2016-01-22'),
(273, 'San Marino (UTC+01:00)', 'SM ', '{"en":"San Marino (UTC+01:00)"}', 'Europe/San_Marino', '1', '2016-01-22', '2016-01-22'),
(274, 'Saudi Arabia (UTC+03:00)', 'SA ', '{"en":"Saudi Arabia (UTC+03:00)"}', 'Asia/Riyadh', '1', '2016-01-22', '2016-01-22'),
(275, 'Serbia (UTC+01:00)', 'RS ', '{"en":"Serbia (UTC+01:00)"}', 'Europe/Belgrade', '1', '2016-01-22', '2016-01-22'),
(276, 'Seychelles (UTC+04:00)', 'SC ', '{"en":"Seychelles (UTC+04:00)"}', 'Indian/Mahe', '1', '2016-01-22', '2016-01-22'),
(277, 'Singapore (UTC+08:00)', 'SG ', '{"en":"Singapore (UTC+08:00)"}', 'Asia/Singapore', '1', '2016-01-22', '2016-01-22'),
(278, 'Slovakia (UTC+01:00)', 'SK ', '{"en":"Slovakia (UTC+01:00)"}', 'Europe/Bratislava', '1', '2016-01-22', '2016-01-22'),
(279, 'Slovenia (UTC+01:00)', 'SI ', '{"en":"Slovenia (UTC+01:00)"}', 'Europe/Ljubljana', '1', '2016-01-22', '2016-01-22'),
(280, 'Solomon Islands (UTC+11:00)', 'SB ', '{"en":"Solomon Islands (UTC+11:00)"}', 'Pacific/Guadalcanal', '1', '2016-01-22', '2016-01-22'),
(281, 'South Africa (UTC+02:00)', 'ZA ', '{"en":"South Africa (UTC+02:00)"}', 'Africa/Johannesburg', '1', '2016-01-22', '2016-01-22'),
(282, 'South Georgia & South Sandwich Islands (UTC-02:00)', 'GS ', '{"en":"South Georgia & South Sandwich Islands (UTC-02:00)"}', 'Atlantic/South_Georgia', '1', '2016-01-22', '2016-01-22'),
(283, 'South Korea (UTC+09:00)', 'KR ', '{"en":"South Korea (UTC+09:00)"}', 'Asia/Seoul', '1', '2016-01-22', '2016-01-22'),
(284, 'Spain (UTC+01:00)', 'ES ', '{"en":"Spain (UTC+01:00)"}', 'Africa/Ceuta', '1', '2016-01-22', '2016-01-22'),
(285, 'Spain (UTC+00:00)', 'ES ', '{"en":"Spain (UTC+00:00)"}', 'Atlantic/Canary', '1', '2016-01-22', '2016-01-22'),
(286, 'Spain (UTC+01:00)', 'ES ', '{"en":"Spain (UTC+01:00)"}', 'Europe/Madrid', '1', '2016-01-22', '2016-01-22'),
(287, 'Sri Lanka (UTC+05:30)', 'LK ', '{"en":"Sri Lanka (UTC+05:30)"}', 'Asia/Colombo', '1', '2016-01-22', '2016-01-22'),
(288, 'St. Pierre & Miquelon (UTC-03:00)', 'PM ', '{"en":"St. Pierre & Miquelon (UTC-03:00)"}', 'America/Miquelon', '1', '2016-01-22', '2016-01-22'),
(289, 'Sudan (UTC+03:00)', 'SD ', '{"en":"Sudan (UTC+03:00)"}', 'Africa/Khartoum', '1', '2016-01-22', '2016-01-22'),
(290, 'Suriname (UTC-03:00)', 'SR ', '{"en":"Suriname (UTC-03:00)"}', 'America/Paramaribo', '1', '2016-01-22', '2016-01-22'),
(291, 'Svalbard & Jan Mayen (UTC+01:00)', 'SJ ', '{"en":"Svalbard & Jan Mayen (UTC+01:00)"}', 'Arctic/Longyearbyen', '1', '2016-01-22', '2016-01-22'),
(292, 'Sweden (UTC+01:00)', 'SE ', '{"en":"Sweden (UTC+01:00)"}', 'Europe/Stockholm', '1', '2016-01-22', '2016-01-22'),
(293, 'Switzerland (UTC+01:00)', 'CH ', '{"en":"Switzerland (UTC+01:00)"}', 'Europe/Zurich', '1', '2016-01-22', '2016-01-22'),
(294, 'Syria (UTC+02:00)', 'SY ', '{"en":"Syria (UTC+02:00)"}', 'Asia/Damascus', '1', '2016-01-22', '2016-01-22'),
(295, 'Taiwan (UTC+08:00)', 'TW ', '{"en":"Taiwan (UTC+08:00)"}', 'Asia/Taipei', '1', '2016-01-22', '2016-01-22'),
(296, 'Tajikistan (UTC+05:00)', 'TJ ', '{"en":"Tajikistan (UTC+05:00)"}', 'Asia/Dushanbe', '1', '2016-01-22', '2016-01-22'),
(297, 'Thailand (UTC+07:00)', 'TH ', '{"en":"Thailand (UTC+07:00)"}', 'Asia/Bangkok', '1', '2016-01-22', '2016-01-22'),
(298, 'Timor-Leste (UTC+09:00)', 'TL ', '{"en":"Timor-Leste (UTC+09:00)"}', 'Asia/Dili', '1', '2016-01-22', '2016-01-22'),
(299, 'Tokelau (UTC+13:00)', 'TK ', '{"en":"Tokelau (UTC+13:00)"}', 'Pacific/Fakaofo', '1', '2016-01-22', '2016-01-22'),
(300, 'Tonga (UTC+13:00)', 'TO ', '{"en":"Tonga (UTC+13:00)"}', 'Pacific/Tongatapu', '1', '2016-01-22', '2016-01-22'),
(301, 'Trinidad & Tobago (UTC-04:00)', 'TT ', '{"en":"Trinidad & Tobago (UTC-04:00)"}', 'America/Port_of_Spain', '1', '2016-01-22', '2016-01-22'),
(302, 'Tunisia (UTC+01:00)', 'TN ', '{"en":"Tunisia (UTC+01:00)"}', 'Africa/Tunis', '1', '2016-01-22', '2016-01-22'),
(303, 'Turkey (UTC+02:00)', 'TR ', '{"en":"Turkey (UTC+02:00)"}', 'Europe/Istanbul', '1', '2016-01-22', '2016-01-22'),
(304, 'Turkmenistan (UTC+05:00)', 'TM ', '{"en":"Turkmenistan (UTC+05:00)"}', 'Asia/Ashgabat', '1', '2016-01-22', '2016-01-22'),
(305, 'Turks & Caicos Islands (UTC-04:00)', 'TC ', '{"en":"Turks & Caicos Islands (UTC-04:00)"}', 'America/Grand_Turk', '1', '2016-01-22', '2016-01-22'),
(306, 'Tuvalu (UTC+12:00)', 'TV ', '{"en":"Tuvalu (UTC+12:00)"}', 'Pacific/Funafuti', '1', '2016-01-22', '2016-01-22'),
(307, 'U.S. Outlying Islands (UTC-10:00)', 'UM ', '{"en":"U.S. Outlying Islands (UTC-10:00)"}', 'Pacific/Johnston', '1', '2016-01-22', '2016-01-22'),
(308, 'U.S. Outlying Islands (UTC-11:00)', 'UM ', '{"en":"U.S. Outlying Islands (UTC-11:00)"}', 'Pacific/Midway', '1', '2016-01-22', '2016-01-22'),
(309, 'U.S. Outlying Islands (UTC+12:00)', 'UM ', '{"en":"U.S. Outlying Islands (UTC+12:00)"}', 'Pacific/Wake', '1', '2016-01-22', '2016-01-22'),
(310, 'Ukraine (UTC+02:00)', 'UA ', '{"en":"Ukraine (UTC+02:00)"}', 'Europe/Kiev', '1', '2016-01-22', '2016-01-22'),
(311, 'Ukraine (UTC+02:00)', 'UA ', '{"en":"Ukraine (UTC+02:00)"}', 'Europe/Uzhgorod', '1', '2016-01-22', '2016-01-22'),
(312, 'Ukraine (UTC+02:00)', 'UA ', '{"en":"Ukraine (UTC+02:00)"}', 'Europe/Zaporozhye', '1', '2016-01-22', '2016-01-22'),
(313, 'United Arab Emirates (UTC+04:00)', 'AE ', '{"en":"United Arab Emirates (UTC+04:00)"}', 'Asia/Dubai', '1', '2016-01-22', '2016-01-22'),
(314, 'United Kingdom (UTC+00:00)', 'GB ', '{"en":"United Kingdom (UTC+00:00)"}', 'Europe/London', '1', '2016-01-22', '2016-01-22'),
(315, 'United States (UTC-10:00)', 'US ', '{"en":"United States (UTC-10:00)"}', 'America/Adak', '1', '2016-01-22', '2016-01-22'),
(316, 'United States (UTC-09:00)', 'US ', '{"en":"United States (UTC-09:00)"}', 'America/Anchorage', '1', '2016-01-22', '2016-01-22'),
(317, 'United States (UTC-07:00)', 'US ', '{"en":"United States (UTC-07:00)"}', 'America/Boise', '1', '2016-01-22', '2016-01-22'),
(318, 'United States (UTC-06:00)', 'US ', '{"en":"United States (UTC-06:00)"}', 'America/Chicago', '1', '2016-01-22', '2016-01-22'),
(319, 'United States (UTC-07:00)', 'US ', '{"en":"United States (UTC-07:00)"}', 'America/Denver', '1', '2016-01-22', '2016-01-22'),
(320, 'United States (UTC-05:00)', 'US ', '{"en":"United States (UTC-05:00)"}', 'America/Detroit', '1', '2016-01-22', '2016-01-22'),
(321, 'United States (UTC-05:00)', 'US ', '{"en":"United States (UTC-05:00)"}', 'America/Indiana/Indianapolis', '1', '2016-01-22', '2016-01-22'),
(322, 'United States (UTC-06:00)', 'US ', '{"en":"United States (UTC-06:00)"}', 'America/Indiana/Knox', '1', '2016-01-22', '2016-01-22'),
(323, 'United States (UTC-05:00)', 'US ', '{"en":"United States (UTC-05:00)"}', 'America/Indiana/Marengo', '1', '2016-01-22', '2016-01-22'),
(324, 'United States (UTC-05:00)', 'US ', '{"en":"United States (UTC-05:00)"}', 'America/Indiana/Petersburg', '1', '2016-01-22', '2016-01-22'),
(325, 'United States (UTC-06:00)', 'US ', '{"en":"United States (UTC-06:00)"}', 'America/Indiana/Tell_City', '1', '2016-01-22', '2016-01-22'),
(326, 'United States (UTC-05:00)', 'US ', '{"en":"United States (UTC-05:00)"}', 'America/Indiana/Vevay', '1', '2016-01-22', '2016-01-22'),
(327, 'United States (UTC-05:00)', 'US ', '{"en":"United States (UTC-05:00)"}', 'America/Indiana/Vincennes', '1', '2016-01-22', '2016-01-22'),
(328, 'United States (UTC-05:00)', 'US ', '{"en":"United States (UTC-05:00)"}', 'America/Indiana/Winamac', '1', '2016-01-22', '2016-01-22'),
(329, 'United States (UTC-09:00)', 'US ', '{"en":"United States (UTC-09:00)"}', 'America/Juneau', '1', '2016-01-22', '2016-01-22'),
(330, 'United States (UTC-05:00)', 'US ', '{"en":"United States (UTC-05:00)"}', 'America/Kentucky/Louisville', '1', '2016-01-22', '2016-01-22'),
(331, 'United States (UTC-05:00)', 'US ', '{"en":"United States (UTC-05:00)"}', 'America/Kentucky/Monticello', '1', '2016-01-22', '2016-01-22'),
(332, 'United States (UTC-08:00)', 'US ', '{"en":"United States (UTC-08:00)"}', 'America/Los_Angeles', '1', '2016-01-22', '2016-01-22'),
(333, 'United States (UTC-06:00)', 'US ', '{"en":"United States (UTC-06:00)"}', 'America/Menominee', '1', '2016-01-22', '2016-01-22'),
(334, 'United States (UTC-08:00)', 'US ', '{"en":"United States (UTC-08:00)"}', 'America/Metlakatla', '1', '2016-01-22', '2016-01-22'),
(335, 'United States (UTC-05:00)', 'US ', '{"en":"United States (UTC-05:00)"}', 'America/New_York', '1', '2016-01-22', '2016-01-22'),
(336, 'United States (UTC-09:00)', 'US ', '{"en":"United States (UTC-09:00)"}', 'America/Nome', '1', '2016-01-22', '2016-01-22'),
(337, 'United States (UTC-06:00)', 'US ', '{"en":"United States (UTC-06:00)"}', 'America/North_Dakota/Beulah', '1', '2016-01-22', '2016-01-22'),
(338, 'United States (UTC-06:00)', 'US ', '{"en":"United States (UTC-06:00)"}', 'America/North_Dakota/Center', '1', '2016-01-22', '2016-01-22'),
(339, 'United States (UTC-06:00)', 'US ', '{"en":"United States (UTC-06:00)"}', 'America/North_Dakota/New_Salem', '1', '2016-01-22', '2016-01-22'),
(340, 'United States (UTC-07:00)', 'US ', '{"en":"United States (UTC-07:00)"}', 'America/Phoenix', '1', '2016-01-22', '2016-01-22'),
(341, 'United States (UTC-09:00)', 'US ', '{"en":"United States (UTC-09:00)"}', 'America/Sitka', '1', '2016-01-22', '2016-01-22'),
(342, 'United States (UTC-09:00)', 'US ', '{"en":"United States (UTC-09:00)"}', 'America/Yakutat', '1', '2016-01-22', '2016-01-22'),
(343, 'United States (UTC-10:00)', 'US ', '{"en":"United States (UTC-10:00)"}', 'Pacific/Honolulu', '1', '2016-01-22', '2016-01-22'),
(344, 'Uruguay (UTC-03:00)', 'UY ', '{"en":"Uruguay (UTC-03:00)"}', 'America/Montevideo', '1', '2016-01-22', '2016-01-22'),
(345, 'Uzbekistan (UTC+05:00)', 'UZ ', '{"en":"Uzbekistan (UTC+05:00)"}', 'Asia/Samarkand', '1', '2016-01-22', '2016-01-22'),
(346, 'Uzbekistan (UTC+05:00)', 'UZ ', '{"en":"Uzbekistan (UTC+05:00)"}', 'Asia/Tashkent', '1', '2016-01-22', '2016-01-22'),
(347, 'Vanuatu (UTC+11:00)', 'VU ', '{"en":"Vanuatu (UTC+11:00)"}', 'Pacific/Efate', '1', '2016-01-22', '2016-01-22'),
(348, 'Vatican City (UTC+01:00)', 'VA ', '{"en":"Vatican City (UTC+01:00)"}', 'Europe/Vatican', '1', '2016-01-22', '2016-01-22'),
(349, 'Venezuela (UTC-04:30)', 'VE ', '{"en":"Venezuela (UTC-04:30)"}', 'America/Caracas', '1', '2016-01-22', '2016-01-22'),
(350, 'Vietnam (UTC+07:00)', 'VN ', '{"en":"Vietnam (UTC+07:00)"}', 'Asia/Ho_Chi_Minh', '1', '2016-01-22', '2016-01-22'),
(351, 'Wallis & Futuna (UTC+12:00)', 'WF ', '{"en":"Wallis & Futuna (UTC+12:00)"}', 'Pacific/Wallis', '1', '2016-01-22', '2016-01-22'),
(352, 'Western Sahara (UTC+00:00)', 'EH ', '{"en":"Western Sahara (UTC+00:00)"}', 'Africa/El_Aaiun', '1', '2016-01-22', '2016-01-22'),
(353, 'UTC (UTC+00:00)', 'UTC', '{"en":"UTC (UTC+00:00)"}', 'UTC', '1', '2016-01-22', '2016-01-22');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `customer`
--

CREATE TABLE `customer` (
  `id` int(10) UNSIGNED NOT NULL,
  `companyname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` int(11) NOT NULL,
  `details` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `published` enum('1','0') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `updated_at` date NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `customer`
--

INSERT INTO `customer` (`id`, `companyname`, `country`, `details`, `email`, `logo`, `published`, `updated_at`, `created_at`) VALUES
(1, 'Makro', 0, 'sadsad', 'sdf@hh.com', '14183927.jpg', '0', '2016-04-25', '2016-04-25'),
(2, 'hola', 0, 'asdasd', 'asdasd@gmail.com', '6vg52v.jpg', '0', '2016-04-25', '2016-04-25'),
(3, 'Microsoft', 0, 'werwer', 'ewrwer@sdfd.com', 'a6.jpg', '0', '2016-04-25', '2016-04-25'),
(4, 'Microsoft', 202, '231321321', 'hemberfer@gmail.com', 'a5.jpg', '1', '2016-04-26', '2016-04-26'),
(5, 'luisasidasd', 18, 'dasdf', 'hemberfer@gmail.com', 'a6.jpg', '1', '2016-04-26', '2016-04-26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_usersystem_table', 1),
('2014_10_12_100000_create_passwordresets_table', 1),
('2016_04_18_144528_create_card_table', 1),
('2016_04_18_145321_create_binnacle_table', 1),
('2016_04_18_145321_create_cardselect_table', 1),
('2016_04_18_145321_create_category_table', 1),
('2016_04_18_145321_create_customer_table', 1),
('2016_04_18_145321_create_permissions_table', 1),
('2016_04_18_145321_create_player_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `passwordresets`
--

CREATE TABLE `passwordresets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `createdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `usersystem` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `player`
--

CREATE TABLE `player` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer` int(11) NOT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `detailsprofile` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('active','inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usersystem`
--

CREATE TABLE `usersystem` (
  `id` int(10) UNSIGNED NOT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `detailsprofile` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('active','inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `binnacle`
--
ALTER TABLE `binnacle`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `card`
--
ALTER TABLE `card`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cardselect`
--
ALTER TABLE `cardselect`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `country_isos`
--
ALTER TABLE `country_isos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `passwordresets`
--
ALTER TABLE `passwordresets`
  ADD KEY `passwordresets_email_index` (`email`),
  ADD KEY `passwordresets_token_index` (`token`);

--
-- Indices de la tabla `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `player`
--
ALTER TABLE `player`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `player_email_unique` (`email`);

--
-- Indices de la tabla `usersystem`
--
ALTER TABLE `usersystem`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `usersystem_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `binnacle`
--
ALTER TABLE `binnacle`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `card`
--
ALTER TABLE `card`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `cardselect`
--
ALTER TABLE `cardselect`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `category`
--
ALTER TABLE `category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `player`
--
ALTER TABLE `player`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `usersystem`
--
ALTER TABLE `usersystem`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
