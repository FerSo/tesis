-- phpMyAdmin SQL Dump
-- version 4.6.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 31-05-2016 a las 21:02:59
-- Versión del servidor: 5.7.12-log
-- Versión de PHP: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `foro`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `archivo_foro`
--

CREATE TABLE `archivo_foro` (
  `id_archivo` int(11) NOT NULL COMMENT 'Con este dato se identifica el archivo',
  `id_foro` int(11) NOT NULL COMMENT 'Indica a que foro esta relacionado el archivo',
  `ubicacion` varchar(50) NOT NULL COMMENT 'Direccion fisica donde esta almacenado el archivo'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Se almacenaran los archivos por foro';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `archivo_respuesta`
--

CREATE TABLE `archivo_respuesta` (
  `id_archivo` int(11) NOT NULL COMMENT 'Identificador del archivo',
  `id_respuesta` int(11) NOT NULL COMMENT 'respuesta a la que esta relacionada el archivo',
  `ubicacion` varchar(255) NOT NULL COMMENT '	Direccion fisica donde esta almacenado el archivo'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='se almacenaras los archivos por respuesta';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bitacora`
--

CREATE TABLE `bitacora` (
  `id_bitacora` int(11) NOT NULL COMMENT 'Identificacion del evento',
  `id_usuario` int(30) NOT NULL COMMENT 'Usuario relacionado con el evento',
  `fecha` datetime NOT NULL COMMENT 'Dia en que ocurrio el evento',
  `modulo` varchar(20) NOT NULL COMMENT 'Area del sistema donde ocurrio el evento',
  `accion` varchar(50) NOT NULL COMMENT 'Descripcion del evento ocurrido'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Guarda los eventos realizados en el sistema';

--
-- Volcado de datos para la tabla `bitacora`
--

INSERT INTO `bitacora` (`id_bitacora`, `id_usuario`, `fecha`, `modulo`, `accion`) VALUES
(1, 1, '2016-05-30 16:08:02', 'Administrar', 'Modificacion Del Usuario: clandor'),
(2, 1, '2016-05-30 16:08:34', 'Administrar', 'Modificacion Del Usuario: jimena'),
(3, 1, '2016-05-30 16:08:42', 'Administrar', 'Modificacion Del Usuario: Hemberfr'),
(4, 15, '2016-05-30 16:21:19', 'Administrar', 'Modificacion Del Usuario: clandor'),
(5, 15, '2016-05-30 16:21:25', 'Administrar', 'Modificacion Del Usuario: jimena'),
(6, 2, '2016-05-30 16:25:48', 'Administrar', 'Modificacion Del Usuario: hemberfer'),
(7, 2, '2016-05-30 18:53:34', 'Administrar', 'Modificacion Del Usuario: clandor'),
(8, 2, '2016-05-30 18:54:37', 'Administrar', 'Modificacion Del Usuario: trimix'),
(9, 1, '2016-05-30 18:54:57', 'Administrar', 'Modificacion Del Usuario: clandor'),
(10, 2, '2016-05-30 23:34:55', 'configración', 'Actualización del perfil '),
(11, 2, '2016-05-30 23:36:11', 'configración', 'Actualización del perfil '),
(12, 2, '2016-05-30 23:49:53', 'configración', 'Cambio de Contraseña del usuario hemberfer'),
(13, 2, '2016-05-30 23:50:57', 'configración', 'Cambio de Contraseña del usuario hemberfer'),
(14, 1, '2016-05-31 00:05:33', 'configración', 'Cambio de Contraseña del usuario clandor'),
(15, 1, '2016-05-31 00:07:37', 'configración', 'Cambio de Contraseña del usuario clandor'),
(16, 1, '2016-05-31 00:08:44', 'configración', 'Cambio de Contraseña del usuario clandor'),
(17, 1, '2016-05-31 00:08:59', 'configración', 'Cambio de Contraseña del usuario clandor'),
(18, 1, '2016-05-31 00:09:58', 'configración', 'Cambio de Contraseña del usuario clandor'),
(19, 2, '2016-05-31 00:14:58', 'configración', 'Actualización del perfil '),
(20, 2, '2016-05-31 00:15:26', 'configración', 'Actualización del perfil '),
(21, 15, '2016-05-31 01:38:29', 'configración', 'Actualización del perfil '),
(22, 1, '2016-05-31 01:39:31', 'configuración', 'Cambio de Contraseña del usuario clandor'),
(23, 1, '2016-05-31 01:55:50', 'configuración', 'Se cambio la imagen del usuario clandor'),
(24, 1, '2016-05-31 01:56:50', 'configuración', 'Se cambio la imagen del usuario clandor'),
(25, 1, '2016-05-31 02:00:20', 'configuración', 'Se cambio la imagen del usuario clandor'),
(26, 1, '2016-05-31 02:01:49', 'configuración', 'Se cambio la imagen del usuario clandor'),
(27, 1, '2016-05-31 08:11:56', 'configuración', 'Se cambio la imagen del usuario clandor'),
(28, 1, '2016-05-31 08:12:31', 'configuración', 'Se cambio la imagen del usuario clandor'),
(29, 1, '2016-05-31 08:13:34', 'configuración', 'Se cambio la imagen del usuario clandor'),
(30, 2, '2016-05-31 08:24:26', 'configuración', 'Se cambio la imagen del usuario hemberfer'),
(31, 2, '2016-05-31 08:24:42', 'configuración', 'Se cambio la imagen del usuario hemberfer'),
(32, 2, '2016-05-31 08:24:54', 'configuración', 'Se cambio la imagen del usuario hemberfer'),
(33, 2, '2016-05-31 08:25:07', 'configuración', 'Se cambio la imagen del usuario hemberfer'),
(34, 2, '2016-05-31 11:11:50', 'configración', 'Actualización del perfil '),
(35, 2, '2016-05-31 11:12:52', 'configuración', 'Cambio de Contraseña del usuario hemberfer'),
(36, 2, '2016-05-31 11:15:45', 'configuración', 'Se cambio la imagen del usuario hemberfer'),
(37, 2, '2016-05-31 11:16:08', 'configuración', 'Se cambio la imagen del usuario hemberfer'),
(38, 2, '2016-05-31 11:18:18', 'configuración', 'Se cambio la imagen del usuario hemberfer'),
(39, 2, '2016-05-31 11:18:44', 'configuración', 'Se cambio la imagen del usuario hemberfer'),
(40, 2, '2016-05-31 11:18:54', 'configuración', 'Se cambio la imagen del usuario hemberfer'),
(41, 1, '2016-05-31 11:20:24', 'configuración', 'Se cambio la imagen del usuario clandor'),
(42, 1, '2016-05-31 11:21:08', 'configuración', 'Se cambio la imagen del usuario clandor'),
(43, 2, '2016-05-31 12:20:30', 'configuración', 'Se cambio la imagen del usuario hemberfer'),
(44, 1, '2016-05-31 12:36:05', 'Administrar', 'Modificacion Del Usuario: clandor'),
(45, 1, '2016-05-31 12:36:18', 'Administrar', 'Modificacion Del Usuario: clandor'),
(46, 1, '2016-05-31 12:36:28', 'Administrar', 'Modificacion Del Usuario: clandor'),
(47, 1, '2016-05-31 13:17:25', 'configración', 'Actualización del perfil '),
(48, 1, '2016-05-31 13:22:18', 'Administrar', 'Modificacion Del Usuario: hemberfer'),
(49, 1, '2016-05-31 13:26:44', 'configración', 'Actualización del perfil '),
(50, 4, '2016-05-31 13:38:52', 'configración', 'Actualización del perfil '),
(51, 2, '2016-05-31 13:40:32', 'configración', 'Actualización del perfil '),
(52, 15, '2016-05-31 14:02:51', 'configuración', 'Se cambio la imagen del usuario trimix'),
(53, 14, '2016-05-31 14:19:13', 'configuración', 'Se cambio la imagen del usuario GuilleG25'),
(54, 14, '2016-05-31 14:19:38', 'configuración', 'Se cambio la imagen del usuario GuilleG25'),
(55, 14, '2016-05-31 14:20:50', 'configuración', 'Cambio de Contraseña del usuario GuilleG25'),
(56, 14, '2016-05-31 14:22:12', 'configración', 'Actualización del perfil '),
(57, 14, '2016-05-31 14:23:30', 'configración', 'Actualización del perfil '),
(58, 2, '2016-05-31 14:25:49', 'Administrar', 'Modificacion Del Usuario: GuilleG25'),
(59, 1, '2016-05-31 14:32:07', 'Administrar', 'Modificacion Del Usuario: GuilleG25'),
(60, 1, '2016-05-31 15:09:29', 'Inicio de Sesión', 'Inicio de sesión'),
(61, 15, '2016-05-31 15:09:56', 'Inicio de Sesión', 'Inicio de sesión'),
(62, 15, '2016-05-31 15:11:12', 'configuración', 'Cambio de Contraseña del usuario trimix'),
(63, 15, '2016-05-31 15:11:32', 'Inicio de Sesión', 'Inicio de sesión'),
(64, 1, '2016-05-31 15:12:45', 'Iniciar Sessión', 'Inicio de clandor'),
(65, 1, '2016-05-31 15:13:36', 'Iniciar Sessión', 'Inicio de sesión del usuario clandor'),
(66, 1, '2016-05-31 15:14:06', 'Iniciar Sessión', 'Inicio de sesión'),
(67, 1, '2016-05-31 15:16:39', 'Cerrar Sesión', 'Finalizo Sesión'),
(68, 1, '2016-05-31 15:16:46', 'Iniciar Sessión', 'Inicio de sesión'),
(69, 1, '2016-05-31 15:16:57', 'Cerrar Sesión', 'Finalizo Sesión'),
(70, 15, '2016-05-31 15:17:02', 'Iniciar Sessión', 'Inicio de sesión'),
(71, 15, '2016-05-31 15:18:22', 'Cerrar Sesión', 'Finalizo Sesión'),
(72, 1, '2016-05-31 15:18:56', 'Iniciar Sessión', 'Inicio de sesión'),
(73, 1, '2016-05-31 15:22:00', 'Crear Tema', 'Creo el siguiente tema: probando bitacora'),
(74, 1, '2016-05-31 15:22:31', 'Crear Tema', 'Creo el siguiente tema: Provando que tan largo pue'),
(75, 1, '2016-05-31 15:23:10', 'Crear Tema', 'Creación de tema: Temasooo!!'),
(76, 1, '2016-05-31 15:26:23', 'Tema: ', 'Respondio a este tema. '),
(79, 1, '2016-05-31 15:36:45', 'Cerrar Sesión', 'Finalizo Sesión'),
(78, 1, '2016-05-31 15:29:04', 'Tema', 'Respondio al tema: probando bitacora'),
(80, 1, '2016-05-31 15:42:16', 'Iniciar Sessión', 'Inicio de sesión'),
(81, 1, '2016-05-31 15:42:23', 'Cerrar Sesión', 'Finalizo Sesión'),
(93, 1, '2016-05-31 15:55:49', 'Cerrar Sesión', 'Finalizo Sesión'),
(83, 1, '2016-05-31 15:42:47', 'Iniciar Sessión', 'Inicio de sesión'),
(84, 1, '2016-05-31 15:46:31', 'Cerrar Sesión', 'Finalizo Sesión'),
(86, 1, '2016-05-31 15:46:45', 'Iniciar Sessión', 'Inicio de sesión'),
(87, 1, '2016-05-31 15:50:03', 'Cerrar Sesión', 'Finalizo Sesión'),
(94, 1, '2016-05-31 15:56:26', 'Iniciar Sessión', 'Inicio de sesión'),
(89, 1, '2016-05-31 15:50:18', 'Iniciar Sessión', 'Inicio de sesión'),
(90, 1, '2016-05-31 15:53:33', 'Cerrar Sesión', 'Finalizo Sesión'),
(91, 19, '2016-05-31 15:53:43', 'Resgistrarse', 'Nuevo Usuario: eeeeeeeee'),
(92, 1, '2016-05-31 15:53:47', 'Iniciar Sessión', 'Inicio de sesión'),
(95, 1, '2016-05-31 15:58:22', 'Cerrar Sesión', 'Finalizo Sesión'),
(96, 1, '2016-05-31 15:58:33', 'Iniciar Sessión', 'Inicio de sesión'),
(97, 1, '2016-05-31 16:00:13', 'Administrar -> Usuar', 'Modificacion Del Usuario: hemberfer'),
(98, 1, '2016-05-31 16:06:25', 'Crear Tema', 'Creación de tema: toki'),
(99, 1, '2016-05-31 16:17:51', 'Cerrar Sesión', 'Finalizo Sesión'),
(100, 1, '2016-05-31 16:17:58', 'Iniciar Sessión', 'Inicio de sesión'),
(101, 1, '2016-05-31 16:18:14', 'Cerrar Sesión', 'Finalizo Sesión'),
(102, 1, '2016-05-31 16:21:40', 'Iniciar Sessión', 'Inicio de sesión'),
(103, 1, '2016-05-31 16:42:40', 'Iniciar Sessión', 'Inicio de sesión'),
(104, 1, '2016-05-31 16:53:08', 'Cerrar Sesión', 'Finalizo Sesión'),
(105, 15, '2016-05-31 16:53:13', 'Iniciar Sessión', 'Inicio de sesión'),
(106, 15, '2016-05-31 16:53:26', 'configuración', 'Se cambio la imagen del usuario trimix');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `buzon_sugerencia`
--

CREATE TABLE `buzon_sugerencia` (
  `id_sugerencia` int(11) NOT NULL,
  `mensaje` varchar(200) NOT NULL,
  `correo` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Se almacenan las sugerencias de los usuarios';

--
-- Volcado de datos para la tabla `buzon_sugerencia`
--

INSERT INTO `buzon_sugerencia` (`id_sugerencia`, `mensaje`, `correo`) VALUES
(1, 'holaaa', ''),
(2, 'dsfdfsdf', ''),
(3, 'holaaa', ''),
(4, 'hola', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id_categoria` int(11) NOT NULL,
  `nombre_categoria` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Datos de la categoria';

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id_categoria`, `nombre_categoria`) VALUES
(1, 'Android'),
(2, 'IOS'),
(3, 'Windows'),
(4, 'PHP'),
(30, 'C++'),
(29, 'PhpStorm'),
(28, 'Linux'),
(115, 'Chrome'),
(122, 'Dispositivos'),
(121, 'Netbeans'),
(120, 'VisualStudio'),
(119, 'Vue.js'),
(118, 'JavaScript'),
(117, 'JQuery');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `foro`
--

CREATE TABLE `foro` (
  `id_foro` int(11) NOT NULL,
  `id_usuario` varchar(30) NOT NULL COMMENT 'Usuario que creo el foro',
  `titulo` varchar(200) NOT NULL,
  `mensaje` text NOT NULL,
  `fecha` datetime NOT NULL,
  `id_categoria` int(11) NOT NULL COMMENT 'Seccion donde esta ubicado el foro',
  `restriccion` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Se guarda la información de los post';

--
-- Volcado de datos para la tabla `foro`
--

INSERT INTO `foro` (`id_foro`, `id_usuario`, `titulo`, `mensaje`, `fecha`, `id_categoria`, `restriccion`) VALUES
(1, '2', 'Ayundenme con mi tablet', 'por favot mi tablet se quedo en el logo de android que puedo hacer?', '2016-05-14 01:21:16', 1, '1'),
(2, '1', 'Como Instalar windows 10', 'Hace tiempo que vi que  ha salido al mercado el nuevo windows y que es gratuito!!! me gustaria saber como obtenerlo.', '2016-05-14 01:22:24', 3, '1'),
(3, '2', 'Mi Libro', 'Luna de pluton esta siendo un exito en todos los pises de habla ispana por favor leelo :v', '2016-05-14 01:22:39', 1, '1'),
(4, '1', 'Probando substr', 'Probando substrProbando substrProbando substrProbando substrProbando substrProbando substrProbando substrProbando substrProbando substrProbando substrProbando substrProbando substrProbando substrProba', '2016-05-14 01:39:57', 1, '1'),
(5, '1', 'Foro largo ', 'Foro largo Foro largo Foro largo Foro largo Foro largo Foro largo Foro largo Foro largo Foro largo Foro largo Foro largo Foro largo Foro largo Foro largo Foro largo Foro largo Foro largo Foro largo Fo', '2016-05-14 01:49:29', 1, '1'),
(6, '1', 'Foro mas largo ', 'Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo Foro mas largo ', '2016-05-14 01:51:28', 1, '1'),
(7, '1', 'Foro solo para Usuarios', 'Esta es una prueba de que solo los usuarios con sesión iniciada pueden ver, esta restricción se realizar al momento de crear foro colocandolo de forma privada.', '2016-05-14 01:55:03', 4, '0'),
(10, '1', 'El mejor', 'Definitivamente', '2016-05-27 23:53:18', 29, '1'),
(9, '1', 'hola hola hola hola hola hola ', 'saf', '2016-05-27 22:52:42', 1, '1'),
(11, '1', 'dsgfsd', 'gsfg', '2016-05-27 23:53:44', 122, '1'),
(12, '1', 'cvxb', 'bcxvb', '2016-05-28 00:43:04', 115, '1'),
(13, '1', 'hfghdf', 'dhfhfd', '2016-05-28 00:49:10', 28, '1'),
(14, '1', 'sfgdsfgdsf', 'gdsgdsf', '2016-05-28 00:49:28', 119, '1'),
(15, '1', 'kjkhj', 'kjhkh', '2016-05-28 00:50:24', 30, '1'),
(16, '1', 'kjljkl', 'lkhk', '2016-05-28 00:50:40', 117, '1'),
(17, '1', 'hjljklhj', 'ljklhkjlhj', '2016-05-28 00:51:22', 120, '1'),
(18, '1', 'mlklkj', 'kjljkljkl', '2016-05-28 00:54:37', 2, '1'),
(19, '1', 'net', 'fdfdfdsgfdhdfhgfjfhjh', '2016-05-28 00:56:20', 121, '1'),
(21, '1', 'Tema sobre chrome', 'Google Chrome es un navegador web desarrollado por Google y compilado con base en varios componentes e infraestructuras de desarrollo de aplicaciones (frameworks) de código abierto,1 como el motor de renderizado Blink (bifurcación o fork de WebKit).2 3 Está disponible gratuitamente bajo condiciones específicas del software privativo o cerrado.4 El nombre del navegador deriva del término en inglés usado para el marco de la interfaz gráfica de usuario («chrome»).\r\n\r\nCuenta con más de 750 millones de usuarios,5 y dependiendo de la fuente de medición global, puede ser considerado el navegador más usado de la Web variando hasta el segundo puesto, algunas veces logrando la popularidad mundial en la primera posición.6 Su cuota de mercado se situaba aproximadamente entre el 17 % y 32 % a finales de junio de 2012,7 8 9 con particular éxito en la mayoría de países de América Latina donde es el más popular.10 Actualmente el número de usuarios aumentó considerablemente situándose en una cuota de mercado cercana al 43 % convirtiéndolo en el navegador más utilizado de todo el planeta.\r\n\r\nPor su parte, Chromium es el proyecto de software libre con el que se ha desarrollado Google Chrome y es de participación comunitaria (bajo el ámbito de Google Code) para fundamentar las bases del diseño y desarrollo del navegador Chrome (junto con la extensión Chrome Frame), además del sistema operativo Google Chrome OS.11 La porción realizada por Google está amparada por la licencia de uso BSD, con otras partes sujetas a una variedad de licencias de código abierto permisivas que incluyen MIT License, Ms-PL y la triple licencia MPL/GPL/LGPL.12 En esencia, los aportes hechos por el proyecto libre Chromium fundamentan el código fuente del navegador base sobre el que está construido Chrome y por tanto tendrá sus mismas características, a las cuales Google adiciona otras que no son software libre. También se cambia el nombre y logotipo por otros ligeramente diferentes para proteger la marca comercial de Google. El resultado se publica bajo términos de software privativo.4 De acuerdo a la documentación para desarrolladores, «“Chromium” es el nombre del proyecto, no del producto, y no debería aparecer nunca entre las variables del código, nombres de APIs, etc. Utilícese “chrome” en su lugar».13\r\n\r\nEl 2 de septiembre de 2008 salió a la luz la primera versión al mercado, siendo esta una versión beta.14 Finalmente, el 11 de diciembre de 2008 se lanzó una versión estable al público en general.15 Actualmente el navegador está disponible para Windows, OS X, Linux, Android y iOS.', '2016-05-28 01:01:29', 115, '1'),
(22, '1', 'Tema sobre Android', 'Android es un sistema operativo basado en el núcleo Linux. Fue diseñado principalmente para dispositivos móviles con pantalla táctil, como teléfonos inteligentes, tablets o tabléfonos; y también para relojes inteligentes, televisores y automóviles. Inicialmente fue desarrollado por Android Inc., empresa que Google respaldó económicamente y más tarde, en 2005, la compró.9 Android fue presentado en 2007 junto la fundación del Open Handset Alliance (un consorcio de compañías de hardware, software y telecomunicaciones) para avanzar en los estándares abiertos de los dispositivos móviles.10 El primer móvil con el sistema operativo Android fue el HTC Dream y se vendió en octubre de 2008.11 Los dispositivos de Android venden más que las ventas combinadas de Windows Phone e IOS.12 13 14 15\r\n\r\nEl éxito del sistema operativo se ha convertido en objeto de litigios sobre patentes en el marco de las llamadas «Guerras por patentes de teléfonos inteligentes» (en inglés, Smartphone patent wars) entre las empresas de tecnología.16 17 Según documentos secretos filtrados en 2013 y 2014, el sistema operativo es uno de los objetivos de las agencias de inteligencia internacionales.\r\n\r\nLa versión básica de Android es conocida como Android Open Source Project (AOSP).18\r\n\r\nEl 25 de junio de 2014 en la Conferencia de Desarrolladores Google I/O, Google mostró una evolución de la marca Android, con el fin de unificar tanto el hardware como el software y ampliar mercados.', '2016-05-28 01:16:00', 1, '1'),
(23, '2', 'sdasdasd', '<b><i><u>holaaaaa</u></i></b>', '2016-05-28 01:29:10', 1, '1'),
(26, '2', 'aiudaaa', '<ul><ul><ul><ul><ul><ul><ul><ul><ul><ul><ul><ul><ul><li style="text-align: left;"><i><b style="background-color: rgb(0, 51, 153);"><font color="#336600">hola por favora aiuda</font></b></i></li></ul></ul></ul></ul></ul></ul></ul></ul></ul></ul></ul></ul></ul>', '2016-05-28 01:54:59', 1, '1'),
(25, '2', '´´´´´´', 'gd<div><br></div><div><br></div><div><img src="http://i.imgur.com/ZhN80FE.jpg" width="752"></div>', '2016-05-28 01:38:18', 1, '1'),
(27, '1', 'Curso de Laravel', 'Curso Introductorio de<b> Laravel</b><div><b>&nbsp;</b><img src="http://i.imgur.com/MUGTsS7.jpg" width="236"></div>', '2016-05-30 14:14:19', 4, '1'),
(28, '4', 'golaaaa', '<h2 style="text-align: center;"><b><i><u><font size="6" face="courier new">Hola brutos</font></u></i></b></h2>', '2016-05-30 20:57:59', 121, '1'),
(30, '2', 'holaasss', 'saassa', '2016-05-31 00:19:42', 122, '1'),
(31, '1', 'Hola papus', '<div align="center"><h5><font face="georgia" size="6"><b><u><i><font color="#33FF99"><u>Tokio Ghoul</u></font><img src="http://i.imgur.com/vplyOEC.jpg" width="1132"></i></u></b></font></h5></div>', '2016-05-31 00:41:01', 28, '1'),
(32, '1', 'Windows 31.05.2016', 'Prueba<br>', '2016-05-31 08:16:04', 3, '0'),
(33, '2', 'fgdfgdfg', '<img src="http://i.imgur.com/WimYW5I.png" width="128"><br>', '2016-05-31 11:17:14', 1, '1'),
(34, '2', 'holaaa', 'sdfdsfd<br>', '2016-05-31 13:09:23', 1, '1'),
(35, '1', 'probando bitacora', 'sdas<br>', '2016-05-31 15:22:00', 30, '1'),
(36, '1', 'Provando que tan largo puede p', 'ssdgsdgdsf<br>', '2016-05-31 15:22:31', 1, '1'),
(37, '1', 'Temasooo!!', 'dsgdsfg<br>', '2016-05-31 15:23:10', 1, '1'),
(38, '1', 'toki', '<div align="left"><img src="http://i.imgur.com/PpzymnU.jpg" height="315" width="560">Holaaaaaaa a todos este es un anime y me gustari aque lo viera c: aadsdf&nbsp; dsg sdfg d et&nbsp; sdgf gds gd <br></div>', '2016-05-31 16:06:25', 115, '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nivel`
--

CREATE TABLE `nivel` (
  `id_nivel` int(11) NOT NULL,
  `nombre_nivel` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Se almacena el nivel de privilegios de los usuarios';

--
-- Volcado de datos para la tabla `nivel`
--

INSERT INTO `nivel` (`id_nivel`, `nombre_nivel`) VALUES
(0, 'Administrador'),
(1, 'Usuario');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `respuestas`
--

CREATE TABLE `respuestas` (
  `id_respuesta` int(11) NOT NULL,
  `id_foro` int(11) NOT NULL COMMENT 'Foro al que esta relacionado la respuesta',
  `fecha` datetime NOT NULL,
  `mensaje` text NOT NULL,
  `id_usuario` int(11) NOT NULL COMMENT 'Usuario que realizo la respuesta'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Se almacena el contenido de las respuestas';

--
-- Volcado de datos para la tabla `respuestas`
--

INSERT INTO `respuestas` (`id_respuesta`, `id_foro`, `fecha`, `mensaje`, `id_usuario`) VALUES
(1, 5, '2016-05-14 00:00:00', 'hola hijos de la chingada', 2),
(2, 3, '2016-05-14 00:00:00', 'hola conchas de su maquina', 2),
(3, 3, '2016-05-14 00:00:00', 'la puta que te pario', 2),
(4, 5, '2016-05-14 00:00:00', 'jajaja', 2),
(5, 6, '2016-05-14 00:00:00', 'fdsfdsf', 1),
(6, 5, '2016-05-14 00:00:00', 'ljfhkdfasdfjkasgdkfasgdjfsadfdsfasdfdsfasdfasdf', 2),
(7, 5, '2016-05-14 00:00:00', 'esta es una prueba de javascript', 2),
(8, 5, '2016-05-14 00:00:00', 'hola estupidos', 2),
(9, 5, '2016-05-14 00:00:00', 'yo opino que la vaina no es asi\r\n', 2),
(10, 0, '2016-05-14 00:00:00', 'tampoco es como tu dices', 2),
(11, 0, '2016-05-14 00:00:00', 'tampoco es como tu dices', 2),
(12, 5, '2016-05-14 00:00:00', '', 2),
(13, 5, '2016-05-14 00:00:00', '', 2),
(14, 5, '2016-05-14 00:00:00', 'a vainaaaaa pssss', 2),
(15, 7, '2016-05-14 00:00:00', 'puto', 2),
(16, 1, '2016-05-14 00:00:00', 'Hola a todos', 1),
(17, 1, '2016-05-14 00:00:00', 'hola amigo como tay?', 2),
(18, 3, '2016-05-14 00:00:00', 'Puto el que lo lea.', 1),
(19, 5, '2016-05-14 00:00:00', 'dfhdgh', 1),
(20, 1, '2016-05-26 00:00:00', 'Hola putitos', 1),
(21, 1, '2016-05-26 00:00:00', 'Que onda wey', 1),
(22, 1, '2016-05-26 00:00:00', 'Que onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda weyQue onda wey', 1),
(23, 1, '2016-05-27 00:00:00', 'hola quiero ese libro :3', 2),
(24, 21, '2016-05-28 00:00:00', 'Chrome para Android e iOS\r\n\r\nEn febrero de 2012, Google lanza \'Chrome para Android Beta\'. Cuya versión está disponible solo para Android 4.0 \'Ice Cream Sandwich\' en teléfonos inteligentes y tabletas. Con la posibilidad de abrir múltiples pestañas, sincronización de marcadores y pestañas con la versión de escritorio, modo incógnito, y ver sitios de uso recientes.21 A finales de junio Chrome para Android se vuelve una versión estable.\r\n\r\nEn junio de 2012, durante el Google I/O 2012, se anuncia que Chrome se utilizará como navegador por defecto en Android 4.1 \'Jelly Bean\' junto con el lanzamiento de la tableta Nexus 7.22 También se anuncia Chrome para iOS, el cual está disponible en iPhone, iPod touch y iPad.23\r\n\r\nEn enero de 2013, las versiones Beta de Chrome también están disponibles en Android para teléfonos y tablets.24 Las cuales son de las versiones más descargadas.', 1),
(25, 21, '2016-05-28 00:00:00', 'Hola que tal \r\n\r\nGracias por tu ayuda jeje', 1),
(26, 26, '2016-05-28 00:00:00', 'holaa en que quieres que te ayude??', 2),
(27, 27, '2016-05-30 00:00:00', 'Tan bello ese perrito :3', 1),
(28, 1, '2016-05-30 00:00:00', 'Hola a todos', 14),
(29, 28, '2016-05-30 20:59:12', 'puto tu', 4),
(30, 28, '2016-05-30 20:59:33', 'adiu bruto tu', 4),
(31, 27, '2016-05-31 11:14:24', 'jajajajajjajajajjajajajajjajajjajajajjajajajjajjajjajjajjaja', 2),
(32, 33, '2016-05-31 11:17:32', 'hola puto', 2),
(33, 1, '2016-05-31 14:00:59', 'Guille es manco', 2),
(34, 37, '2016-05-31 15:26:23', 'sdgdsgds', 1),
(35, 36, '2016-05-31 15:27:34', 'asfsdf', 1),
(36, 35, '2016-05-31 15:29:04', 'sdgsdfg', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL,
  `usuario_nombre` varchar(15) NOT NULL DEFAULT '',
  `avatar` varchar(255) DEFAULT NULL COMMENT 'direccion de la imagen de perfil',
  `nombre` varchar(255) NOT NULL,
  `apellido` varchar(255) NOT NULL,
  `usuario_clave` varchar(32) NOT NULL DEFAULT '',
  `usuario_email` varchar(50) NOT NULL DEFAULT '',
  `nivel` tinyint(1) NOT NULL DEFAULT '1',
  `fecha_reg` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `usuario_nombre`, `avatar`, `nombre`, `apellido`, `usuario_clave`, `usuario_email`, `nivel`, `fecha_reg`) VALUES
(1, 'clandor', 'maxresdefault.jpg', 'Clandor', 'SK', '202cb962ac59075b964b07152d234b70', 'carlosmejias@gmail.com', 0, '2016-05-14 01:17:42'),
(2, 'hemberfer', '1461203399_R.png', 'Hember', 'a coñazos :v', 'e10adc3949ba59abbe56e057f20f883e', 'hemberfer@gmail.com', 0, '2016-05-14 01:17:54'),
(3, 'marigaby', NULL, 'María Gabrielaa', 'Colmenares Soto', '027b3393e88305765ec8319ce36c0b7d', 'marigaby@gmail.com', 1, '2016-05-14 01:26:50'),
(4, 'consuela', NULL, 'Consuelo', 'Soto', '027b3393e88305765ec8319ce36c0b7d', 'mariasoto@gmail.com', 1, '2016-05-26 20:02:16'),
(5, 'josefina', NULL, 'Josefina', 'Garrido', '027b3393e88305765ec8319ce36c0b7d', 'Josefina@gmail.com', 1, '2016-05-26 20:35:18'),
(6, 'margarita', NULL, 'margarita', 'gaitan', '027b3393e88305765ec8319ce36c0b7d', 'hemberfere@gmail.com', 1, '2016-05-26 20:50:37'),
(7, 'margarito', NULL, 'margarita', 'margarita', '027b3393e88305765ec8319ce36c0b7d', 'margarito@gmail.com', 1, '2016-05-26 20:56:50'),
(8, 'jimena', NULL, 'jimena', 'jimena', '027b3393e88305765ec8319ce36c0b7d', 'jimena@hotmail.com', 1, '2016-05-26 21:02:40'),
(9, 'gumersinda', NULL, 'gumersinda', 'gumersinda', '027b3393e88305765ec8319ce36c0b7d', 'gumer@gmail.com', 1, '2016-05-26 21:27:58'),
(10, 'anastasio', NULL, 'anastasio', 'anastasio', '027b3393e88305765ec8319ce36c0b7d', 'anastasio@gmail.com', 1, '2016-05-26 22:03:01'),
(11, 'Mariaaaa', NULL, 'mariaaa', 'mariaaa', '027b3393e88305765ec8319ce36c0b7d', 'mariaaaa@gmail.com', 1, '2016-05-26 22:03:50'),
(12, 'Hemberf', NULL, 'Hember', 'Colmenares', '027b3393e88305765ec8319ce36c0b7d', 'dd@gmail.com', 1, '2016-05-27 23:55:03'),
(13, 'Hemberfr', NULL, 'Hember', 'Colmenares', '027b3393e88305765ec8319ce36c0b7d', 'ddd@gmail.com', 1, '2016-05-27 23:56:18'),
(14, 'GuilleG25', 'Paste (Tue, Apr 12, 2016 8-50 AM).png', 'id_pla', 'Papus :v', '25f9e794323b453885f5181f1b624d0b', 'guillermo_daniel.1994@hotmail.com', 1, '2016-05-30 14:11:24'),
(15, 'trimix', 'madara_uchiha___this_only_are_beginning____by_aosak24-d6vyhpz.png', 'trimix', 'SK', '202cb962ac59075b964b07152d234b70', 'carlos@hotmail.com', 0, '2016-05-30 15:49:53'),
(16, 'carwqf', NULL, 'sdfadf', 'sadfsadf', '202cb962ac59075b964b07152d234b70', 'carlos@hotmail.com', 1, '2016-05-31 15:42:38'),
(17, 'sdhdghd', NULL, 'hfhfdhdh', 'jfdjhfd', '202cb962ac59075b964b07152d234b70', 'carlos@hotmail.com', 1, '2016-05-31 15:46:40'),
(18, 'aaaaaa', NULL, 'aaaaaa', 'aaaaaaa', '202cb962ac59075b964b07152d234b70', 'carlos@hotmail.com', 1, '2016-05-31 15:50:14'),
(19, 'eeeeeeeee', NULL, 'eeeeeeeee', 'eeeeeeeeee', '202cb962ac59075b964b07152d234b70', 'carlos@hotmail.com', 1, '2016-05-31 15:53:42');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `archivo_foro`
--
ALTER TABLE `archivo_foro`
  ADD PRIMARY KEY (`id_archivo`),
  ADD KEY `fk_archivo_foro1` (`id_foro`);

--
-- Indices de la tabla `archivo_respuesta`
--
ALTER TABLE `archivo_respuesta`
  ADD PRIMARY KEY (`id_archivo`);

--
-- Indices de la tabla `bitacora`
--
ALTER TABLE `bitacora`
  ADD PRIMARY KEY (`id_bitacora`),
  ADD KEY `fk_bitacora_usuario1` (`id_usuario`);

--
-- Indices de la tabla `buzon_sugerencia`
--
ALTER TABLE `buzon_sugerencia`
  ADD PRIMARY KEY (`id_sugerencia`);

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indices de la tabla `foro`
--
ALTER TABLE `foro`
  ADD PRIMARY KEY (`id_foro`),
  ADD KEY `fk_foro_categoria` (`id_categoria`);

--
-- Indices de la tabla `nivel`
--
ALTER TABLE `nivel`
  ADD PRIMARY KEY (`id_nivel`);

--
-- Indices de la tabla `respuestas`
--
ALTER TABLE `respuestas`
  ADD PRIMARY KEY (`id_respuesta`),
  ADD KEY `fk_respuestas_foro1` (`id_foro`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `bitacora`
--
ALTER TABLE `bitacora`
  MODIFY `id_bitacora` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificacion del evento', AUTO_INCREMENT=107;
--
-- AUTO_INCREMENT de la tabla `buzon_sugerencia`
--
ALTER TABLE `buzon_sugerencia`
  MODIFY `id_sugerencia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id_categoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=124;
--
-- AUTO_INCREMENT de la tabla `foro`
--
ALTER TABLE `foro`
  MODIFY `id_foro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT de la tabla `respuestas`
--
ALTER TABLE `respuestas`
  MODIFY `id_respuesta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
