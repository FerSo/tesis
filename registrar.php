<!DOCTYPE html>
<html>
<head>
    <?php include("partes/head.php"); ?>
    <title> Foros Informáticos</title>
</head>
<body>
<?php include("partes/barra.php"); ?>
<br><br>


<div class="col-lg-4 col-lg-offset-4 center animated fadeInRight">

    <div class="row subtitle">
        <h1>
            Registrate
        </h1>
    </div>

    <p>Crea una cuenta para compartir..</p>
    <?php
    include('complementos/acceso_db.php'); // incluimos el archivo de conexión a la Base de Datos
    if (isset($_POST['enviar'])) { // comprobamos que se han enviado los datos desde el formulario
        // creamos una función que nos parmita validar el email
        function valida_email($correo)
        {
            if (preg_match('/^[A-Za-z0-9-_.+%]+@[A-Za-z0-9-.]+\.[A-Za-z]{2,4}$/', $correo)) return true;
            else return false;
        }

        // Procedemos a comprobar que los campos del formulario no estén vacíos
        $sin_espacios = count_chars($_POST['usuario_nombre'], 1);
        if (!empty($sin_espacios[32])) { // comprobamos que el campo usuario_nombre no tenga espacios en blanco
            echo "El campo <em>usuario_nombre</em> no debe contener espacios en blanco. <a href='javascript:history.back();'>Reintentar</a>";
        } elseif (empty($_POST['usuario_nombre'])) { // comprobamos que el campo usuario_nombre no esté vacío
            echo "No haz ingresado tu usuario. <a href='javascript:history.back();'>Reintentar</a>";
        } elseif (empty($_POST['nombre'])) { // comprobamos que el campo nombre no esté vacío
            echo "No haz ingresado tu nombre. <a href='javascript:history.back();'>Reintentar</a>";
        } elseif (empty($_POST['apellido'])) { // comprobamos que el campo apellido no esté vacío
            echo "No haz ingresado tu apellido. <a href='javascript:history.back();'>Reintentar</a>";
        } elseif (empty($_POST['usuario_clave'])) { // comprobamos que el campo usuario_clave no esté vacío
            echo "No haz ingresado contraseña. <a href='javascript:history.back();'>Reintentar</a>";
        } elseif ($_POST['usuario_clave'] != $_POST['usuario_clave_conf']) { // comprobamos que las contraseñas ingresadas coincidan
            echo "Las contraseñas ingresadas no coinciden. <a href='javascript:history.back();'>Reintentar</a>";
        } elseif (!valida_email($_POST['usuario_email'])) { // validamos que el email ingresado sea correcto
            echo "El email ingresado no es válido. <a href='javascript:history.back();'>Reintentar</a>";
        } else {

            // "limpiamos" los campos del formulario de posibles códigos maliciosos

            $usuario_nombre = mysql_real_escape_string($_POST['usuario_nombre']);
            $nombre = mysql_real_escape_string($_POST['nombre']);
            $apellido = mysql_real_escape_string($_POST['apellido']);
            $usuario_clave = mysql_real_escape_string($_POST['usuario_clave']);
            $usuario_email = mysql_real_escape_string($_POST['usuario_email']);

            // comprobamos que el usuario ingresado no haya sido registrado antes

            $sql = mysql_query("SELECT usuario_nombre FROM usuarios WHERE usuario_nombre='" . $usuario_nombre . "'");
            if (mysql_num_rows($sql) > 0) {
                echo "El nombre usuario elegido ya ha sido registrado anteriormente. <a href='javascript:history.back();'>Reintentar</a>";
            } else {
                $usuario_clave = md5($usuario_clave); // encriptamos la contraseña ingresada con md5
                // ingresamos los datos a la BD
                $reg = mysql_query("INSERT INTO usuarios (usuario_nombre, nombre, apellido, usuario_clave, usuario_email, fecha_reg) VALUES ('" . $usuario_nombre . "', '" . $nombre . "', '" . $apellido . "', '" . $usuario_clave . "', '" . $usuario_email . "', NOW())");

                $consult = mysql_query("SELECT id_usuario FROM usuarios WHERE usuario_nombre = '" . $usuario_nombre . "'");
                $cons = mysql_fetch_array($consult);


                $modulo = "Resgistrarse";
                $accion = "Nuevo Usuario: " . $usuario_nombre . "";
                $bit = mysql_query("INSERT INTO bitacora (id_usuario, fecha, modulo, accion) VALUES ('" . $cons[0] . "', NOW(),'" . $modulo . "' , '" . $accion . "')");
                if ($bit) {
                    echo "Datos ingresados correctamente.";
                } else {
                    echo "ha ocurrido un error y no se registraron los datos.";
                }


                if ($reg) {
                    echo "Datos ingresados correctamente.  <a class=\"btn-primary btn-xs\" href=\"acceso.php\">Inicia Sesión</a>";
                } else {
                    echo "ha ocurrido un error y no se registraron los datos.";
                }

            }
        }
    } else {
        ?>
        <form action="<?= $_SERVER['PHP_SELF'] ?>" method="post">
            <div class="form-group">
                <input type="text" name="usuario_nombre" class="form-control" placeholder="Nombre de Usuario"
                       maxlength="15" minlength="6" required="">
            </div>
            <div class="form-group">
                <input type="text" name="nombre" class="form-control" placeholder="Nombre" maxlength="15"
                       minlength="3" required="">
            </div>
            <div class="form-group">
                <input type="text" name="apellido" class="form-control" placeholder="Apellido" maxlength="15"
                       minlength="3" required="">
            </div>
            <div class="form-group">
                <input type="password" name="usuario_clave" class="form-control" placeholder="Contraseña"
                       maxlength="15" minlength="6" required="">
            </div>
            <div class="form-group">
                <input type="password" name="usuario_clave_conf" class="form-control"
                       placeholder="Confirmar Contraseña" maxlength="15" minlength="6" required="">
            </div>
            <div class="form-group">
                <input type="email" name="usuario_email" class="form-control" placeholder="Correo Electrónico"
                       maxlength="50" minlength="6" required="">
            </div>
            <input type="submit" name="enviar" value="Registrar" class=" btn-primary"></input><input class="btn"
                                                                                                     style="margin-left: 15px; color: black"
                                                                                                     type="reset"
                                                                                                     value="Limpiar"/>

            <p class="text-muted text-center">
                <small>Ya tienes una cuenta?</small>
            </p>
            <a class="btn-primary btn-xs" href="acceso.php">Inicia Sesión</a>
        </form>
        <?php
    }
    ?>
</div>
<br><br>

<?php include("partes/footer.php"); ?>
<?php include("partes/scripts.php"); ?>
</body>
</html>
