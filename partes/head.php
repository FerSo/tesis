<<<<<<< HEAD
<head>
    <?php date_default_timezone_set('America/Asuncion');?>
    <title>Foros Informáticos</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/font-awesome.min.css" rel="stylesheet">
    <link href="../css/animate.min.css" rel="stylesheet">
    <link href="../css/prettyPhoto.css" rel="stylesheet">
    <link href="../css/main.css" rel="stylesheet">
    <link href="../css/responsive.css" rel="stylesheet">
    <link href="../css/datatables.min.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet">
    <link href="../css/style.min.css" rel="stylesheet">
    <link rel="shortcut icon" href="../images/ico/icon.png">
    <style>
        body {
            background-image: url(../images/fondo5.png);
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-position:  100%;
            background-size: 100% 100%;
        }
    </style>

<style>
    .circular {
        width: 30px;
        height: 30px;
        border-radius: 150px;
        -webkit-border-radius: 150px;
        -moz-border-radius: 150px;
    }

    @import url(http://fonts.googleapis.com/css?family=Cabin:400);

    .webdesigntuts-workshop:before {
        background: #444;
        background: -webkit-linear-gradient(left, #151515, #444, #151515);
        background: -moz-linear-gradient(left, #151515, #444, #151515);
        background: -o-linear-gradient(left, #151515, #444, #151515);
        background: -ms-linear-gradient(left, #151515, #444, #151515);
        background: linear-gradient(left, #151515, #444, #151515);
        top: 192px;
    }

    .webdesigntuts-workshop:after {
        background: #000;
        background: -webkit-linear-gradient(left, #151515, #000, #151515);
        background: -moz-linear-gradient(left, #151515, #000, #151515);
        background: -o-linear-gradient(left, #151515, #000, #151515);
        background: -ms-linear-gradient(left, #151515, #000, #151515);
        background: linear-gradient(left, #151515, #000, #151515);
        top: 191px;
    }

    .webdesigntuts-workshop form {
        background: #111;
        background: -webkit-linear-gradient(#1b1b1b, #111);
        background: -moz-linear-gradient(#1b1b1b, #111);
        background: -o-linear-gradient(#1b1b1b, #111);
        background: -ms-linear-gradient(#1b1b1b, #111);
        background: linear-gradient(#1b1b1b, #111);
        border: 1px solid #000;
        border-radius: 5px;
        box-shadow: inset 0 0 0 1px #272727;
        display: inline-block;
        font-size: 0px;
        margin: 0px auto 0;
        padding: 10px;
        position: relative;
        z-index: 1;
    }

    .webdesigntuts-workshop input {
        background: #222;
        background: -webkit-linear-gradient(#333, #222);
        background: -moz-linear-gradient(#333, #222);
        background: -o-linear-gradient(#333, #222);
        background: -ms-linear-gradient(#333, #222);
        background: linear-gradient(#333, #222);
        border: 1px solid #444;
        border-radius: 5px 0 0 5px;
        box-shadow: 0 2px 0 #000;
        color: #888;
        display: block;
        float: left;
        font-family: 'Cabin', helvetica, arial, sans-serif;
        font-size: 13px;
        font-weight: 400;
        height: 30px;
        margin: 0;
        padding: 0px;
        text-shadow: 0 -1px 0 #000;
        width: 150px;
    }

    .ie .webdesigntuts-workshop input {
        line-height: 40px;
    }

    .webdesigntuts-workshop input::-webkit-input-placeholder {
        color: #888;
    }

    .webdesigntuts-workshop input:-moz-placeholder {
        color: #888;
    }

    .webdesigntuts-workshop input:focus {
        -webkit-animation: glow 800ms ease-out infinite alternate;
        -moz-animation: glow 800ms ease-out infinite alternate;
        -o-animation: glow 800ms ease-out infinite alternate;
        -ms-animation: glow 800ms ease-out infinite alternate;
        animation: glow 800ms ease-out infinite alternate;
        background: #222922;
        background: -webkit-linear-gradient(#333933, #222922);
        background: -moz-linear-gradient(#333933, #222922);
        background: -o-linear-gradient(#333933, #222922);
        background: -ms-linear-gradient(#333933, #222922);
        background: linear-gradient(#333933, #222922);
        border-color: #393;
        box-shadow: 0 0 5px rgba(0, 255, 0, .2), inset 0 0 5px rgba(0, 255, 0, .1), 0 2px 0 #000;
        color: #efe;
        outline: none;
    }

    .webdesigntuts-workshop input:focus::-webkit-input-placeholder {
        color: #efe;
    }

    .webdesigntuts-workshop input:focus:-moz-placeholder {
        color: #efe;
    }

    .webdesigntuts-workshop button {
        background: #222;
        background: -webkit-linear-gradient(#333, #222);
        background: -moz-linear-gradient(#333, #222);
        background: -o-linear-gradient(#333, #222);
        background: -ms-linear-gradient(#333, #222);
        background: linear-gradient(#333, #222);
        -webkit-box-sizing: content-box;
        -moz-box-sizing: content-box;
        -o-box-sizing: content-box;
        -ms-box-sizing: content-box;
        box-sizing: content-box;
        border: 1px solid #444;
        border-left-color: #000;
        border-radius: 0 5px 5px 0;
        box-shadow: 0 2px 0 #000;
        color: #fff;
        float: left;
        font-family: 'Cabin', helvetica, arial, sans-serif;
        font-size: 13px;
        font-weight: 400;
        height: 28px;
        line-height: 40px;
        margin: 0;
        padding: 0;
        position: relative;
        text-shadow: 0 -1px 0 #000;
        width: 50px;
    }

    .webdesigntuts-workshop button:hover,
    .webdesigntuts-workshop button:focus {
        background: #292929;
        background: -webkit-linear-gradient(#393939, #292929);
        background: -moz-linear-gradient(#393939, #292929);
        background: -o-linear-gradient(#393939, #292929);
        background: -ms-linear-gradient(#393939, #292929);
        background: linear-gradient(#393939, #292929);
        color: #;
        outline: none;
    }

    .webdesigntuts-workshop button:active {
        background: #292929;
        background: -webkit-linear-gradient(#393939, #292929);
        background: -moz-linear-gradient(#393939, #292929);
        background: -o-linear-gradient(#393939, #292929);
        background: -ms-linear-gradient(#393939, #292929);
        background: linear-gradient(#393939, #292929);
        box-shadow: 0 1px 0 #000, inset 1px 0 1px #222;
        top: 1px;
    }

    @-webkit-keyframes glow {
        0% {
            border-color: #393;
            box-shadow: 0 0 5px rgba(0, 255, 0, .2), inset 0 0 5px rgba(0, 255, 0, .1), 0 2px 0 #000;
        }
        100% {
            border-color: #6f6;
            box-shadow: 0 0 20px rgba(0, 255, 0, .6), inset 0 0 10px rgba(0, 255, 0, .4), 0 2px 0 #000;
        }
    }

    @-moz-keyframes glow {
        0% {
            border-color: #393;
            box-shadow: 0 0 5px rgba(0, 255, 0, .2), inset 0 0 5px rgba(0, 255, 0, .1), 0 2px 0 #000;
        }
        100% {
            border-color: #6f6;
            box-shadow: 0 0 20px rgba(0, 255, 0, .6), inset 0 0 10px rgba(0, 255, 0, .4), 0 2px 0 #000;
        }
    }

    @-o-keyframes glow {
        0% {
            border-color: #393;
            box-shadow: 0 0 5px rgba(0, 255, 0, .2), inset 0 0 5px rgba(0, 255, 0, .1), 0 2px 0 #000;
        }
        100% {
            border-color: #6f6;
            box-shadow: 0 0 20px rgba(0, 255, 0, .6), inset 0 0 10px rgba(0, 255, 0, .4), 0 2px 0 #000;
        }
    }

    @-ms-keyframes glow {
        0% {
            border-color: #393;
            box-shadow: 0 0 5px rgba(0, 255, 0, .2), inset 0 0 5px rgba(0, 255, 0, .1), 0 2px 0 #000;
        }
        100% {
            border-color: #6f6;
            box-shadow: 0 0 20px rgba(0, 255, 0, .6), inset 0 0 10px rgba(0, 255, 0, .4), 0 2px 0 #000;
        }
    }

    @keyframes glow {
        0% {
            border-color: #393;
            box-shadow: 0 0 5px rgba(0, 255, 0, .2), inset 0 0 5px rgba(0, 255, 0, .1), 0 2px 0 #000;
        }
        100% {
            border-color: #6f6;
            box-shadow: 0 0 20px rgba(0, 255, 0, .6), inset 0 0 10px rgba(0, 255, 0, .4), 0 2px 0 #000;

</style>
  <style>

        /* jssor slider bullet navigator skin 03 css */
        /*
        .jssorb03 div           (normal)
        .jssorb03 div:hover     (normal mouseover)
        .jssorb03 .av           (active)
        .jssorb03 .av:hover     (active mouseover)
        .jssorb03 .dn           (mousedown)
        */
        .jssorb03 {
            position: absolute;
        }
        .jssorb03 div, .jssorb03 div:hover, .jssorb03 .av {
            position: absolute;
            /* size of bullet elment */
            width: 21px;
            height: 21px;
            text-align: center;
            line-height: 21px;
            color: white;
            font-size: 12px;
            background: url('img/b03.png') no-repeat;
            overflow: hidden;
            cursor: pointer;
        }
        .jssorb03 div { background-position: -5px -4px; }
        .jssorb03 div:hover, .jssorb03 .av:hover { background-position: -35px -4px; }
        .jssorb03 .av { background-position: -65px -4px; }
        .jssorb03 .dn, .jssorb03 .dn:hover { background-position: -95px -4px; }

        /* jssor slider arrow navigator skin 03 css */
        /*
        .jssora03l                  (normal)
        .jssora03r                  (normal)
        .jssora03l:hover            (normal mouseover)
        .jssora03r:hover            (normal mouseover)
        .jssora03l.jssora03ldn      (mousedown)
        .jssora03r.jssora03rdn      (mousedown)
        */
        .jssora03l, .jssora03r {
            display: block;
            position: absolute;
            /* size of arrow element */
            width: 55px;
            height: 55px;
            cursor: pointer;
            background: url('img/a03.png') no-repeat;
            overflow: hidden;
        }
        .jssora03l { background-position: -3px -33px; }
        .jssora03r { background-position: -63px -33px; }
        .jssora03l:hover { background-position: -123px -33px; }
        .jssora03r:hover { background-position: -183px -33px; }
        .jssora03l.jssora03ldn { background-position: -243px -33px; }
        .jssora03r.jssora03rdn { background-position: -303px -33px; }
    </style>
    <style>
    .perfil {
        border-radius: 150px;
        -webkit-border-radius: 150px;
        -moz-border-radius: 150px;
    }
</style>
<style>
    .circulares {
        width: 65px;
        height: 65px;
        border-radius: 150px;
        -webkit-border-radius: 150px;
        -moz-border-radius: 150px;
    }
</style>
</head>
=======
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>Foros Informáticos</title>

<!-- core CSS -->
<?php date_default_timezone_set('America/Asuncion');?>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/prettyPhoto.css" rel="stylesheet">
<link href="css/main.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet">
<!--[if lt IE 9]>
<script src="../js/html5shiv.js"></script>
<script src="../js/respond.min.js"></script>
<![endif]-->
<link rel="shortcut icon" href="images/ico/icon.png">
        <style>
            body {
                background-image: url(images/fondo5.png);
                background-repeat: no-repeat;
                background-attachment: fixed;
                background-position: center;
            }
        </style>
>>>>>>> fcb1717f91a1249180f89b0b43f43efeaa517a32
