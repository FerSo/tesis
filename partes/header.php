<header style="margin-bottom: 25px">
    <nav class="navbar navbar-inverse" role="banner">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="../vistas/index.php">
                    <img src="../images/logo.png" alt="logo" style="padding-top: 0px !important;"></a>
            </div>

            <?php
            include('../complementos/acceso_db.php');
            if (isset($_SESSION['usuario_nombre'])) { ?>

                <div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                        <li><a href="../vistas/index.php">Inicio</a></li>
                        <li><a href="../vistas/listaTemas.php">Lista de Temas</a></li>
                        <li><a href="../vistas/crearTema.php">Crear Tema</a></li>
                        <? if ($_SESSION['nivel'] == '0') {
                            $visto = 0;
                            $count = mysql_query("SELECT * FROM buzon_sugerencia WHERE visto = '" . $visto . "'");
                            $numero = mysql_num_rows($count);
                            ?>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Administrar <i
                                        class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="../vistas/listaUsuarios.php">Usuarios</a></li>
                                    <li><a href="../vistas/listaCategorias.php">Categorias</a></li>
                                    <li><a href="../vistas/reportes.php">Reportes</a></li>
                                    <li><a href="../vistas/bitacora.php">Bitacora</a></li>
                                    <li><a href="../vistas/buzonSugerencias.php">Buzón De Sugerencias<br>
                                            <center><?= $numero ?> Mensajes No Leidos</center>
                                        </a></li>
                                </ul>
                            </li>
                        <? } ?>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle"
                               data-toggle="dropdown"><? echo "" . $_SESSION['nombre']; ?> <? echo "" . $_SESSION['apellido']; ?>
                                <i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="../vistas/usuarioTemas.php?id=<?= $_SESSION['id_usuario'] ?>">Mis
                                        Temas</a></li>
                                <li><a href="../vistas/perfil.php?id=<?= $_SESSION['id_usuario'] ?>">Perfil</a></li>
                                <li><a href="../vistas/contacto.php">Contacto</a></li>
                                <li><a href="../complementos/logout.php"><i class="fa fa-sign-out"></i> Cerrar
                                        Sesión</a></li>
                            </ul>
                        </li>
                        <?php

                        $datos = mysql_query("SELECT * FROM usuarios WHERE id_usuario ='" . $_SESSION['id_usuario'] . "'");
                        $dat = mysql_fetch_array($datos);
                        if (is_null($dat[2])) { ?>
                            <div class="circular"
                                 style="float:left; background-image:url(../images/avatar/default.jpg);background-position:50% 50%; background-size:100% 100%;background-repeat: no-repeat;"></div>

                        <? } else { ?>
                            <div class="circular"
                                 style="position:relative;float:left; background-image:url(../images/avatar/<?= $dat[0] ?>/<?= $dat[2] ?>);background-position:50% 50%; background-size:auto 100%;background-repeat: no-repeat; background-color: black"></div>
                        <? } ?>
                    </ul>
                </div>
            <? } else {
                ?>

                <div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                        <li><a href="../vistas/index.php">Inicio</a></li>
                        <li><a href="../vistas/registrar.php">Registrarse</a></li>
                        <li><a href="../vistas/acceso.php">Iniciar Sesión</a></li>
                    </ul>
                </div>

            <? } ?>
            <div class="navbar-header" style="float: left">
                <section class="webdesigntuts-workshop">
                    <form action="" method="">
                        <input type="Buscar" placeholder="Buscar...">
                        <button
                            style="background-image: url(../images/buscar.png); background-position: 50% center; background-size: auto 100%; background-repeat: no-repeat "></button>
                    </form>
                </section>
            </div>
        </div>
    </nav>
</header>