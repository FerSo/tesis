<style>
    .circular {
        width: 30px;
        height: 30px;
        border-radius: 150px;
        -webkit-border-radius: 150px;
        -moz-border-radius: 150px;
    }
<<<<<<< HEAD

    @import url(http://fonts.googleapis.com/css?family=Cabin:400);

    .webdesigntuts-workshop:before {
        background: #444;
        background: -webkit-linear-gradient(left, #151515, #444, #151515);
        background: -moz-linear-gradient(left, #151515, #444, #151515);
        background: -o-linear-gradient(left, #151515, #444, #151515);
        background: -ms-linear-gradient(left, #151515, #444, #151515);
        background: linear-gradient(left, #151515, #444, #151515);
        top: 192px;
    }

    .webdesigntuts-workshop:after {
        background: #000;
        background: -webkit-linear-gradient(left, #151515, #000, #151515);
        background: -moz-linear-gradient(left, #151515, #000, #151515);
        background: -o-linear-gradient(left, #151515, #000, #151515);
        background: -ms-linear-gradient(left, #151515, #000, #151515);
        background: linear-gradient(left, #151515, #000, #151515);
        top: 191px;
    }

    .webdesigntuts-workshop form {
        background: #111;
        background: -webkit-linear-gradient(#1b1b1b, #111);
        background: -moz-linear-gradient(#1b1b1b, #111);
        background: -o-linear-gradient(#1b1b1b, #111);
        background: -ms-linear-gradient(#1b1b1b, #111);
        background: linear-gradient(#1b1b1b, #111);
        border: 1px solid #000;
        border-radius: 5px;
        box-shadow: inset 0 0 0 1px #272727;
        display: inline-block;
        font-size: 0px;
        margin: 0px auto 0;
        padding: 10px;
        position: relative;
        z-index: 1;
    }

    .webdesigntuts-workshop input {
        background: #222;
        background: -webkit-linear-gradient(#333, #222);
        background: -moz-linear-gradient(#333, #222);
        background: -o-linear-gradient(#333, #222);
        background: -ms-linear-gradient(#333, #222);
        background: linear-gradient(#333, #222);
        border: 1px solid #444;
        border-radius: 5px 0 0 5px;
        box-shadow: 0 2px 0 #000;
        color: #888;
        display: block;
        float: left;
        font-family: 'Cabin', helvetica, arial, sans-serif;
        font-size: 13px;
        font-weight: 400;
        height: 30px;
        margin: 0;
        padding: 0px;
        text-shadow: 0 -1px 0 #000;
        width: 150px;
    }

    .ie .webdesigntuts-workshop input {
        line-height: 40px;
    }

    .webdesigntuts-workshop input::-webkit-input-placeholder {
        color: #888;
    }

    .webdesigntuts-workshop input:-moz-placeholder {
        color: #888;
    }

    .webdesigntuts-workshop input:focus {
        -webkit-animation: glow 800ms ease-out infinite alternate;
        -moz-animation: glow 800ms ease-out infinite alternate;
        -o-animation: glow 800ms ease-out infinite alternate;
        -ms-animation: glow 800ms ease-out infinite alternate;
        animation: glow 800ms ease-out infinite alternate;
        background: #222922;
        background: -webkit-linear-gradient(#333933, #222922);
        background: -moz-linear-gradient(#333933, #222922);
        background: -o-linear-gradient(#333933, #222922);
        background: -ms-linear-gradient(#333933, #222922);
        background: linear-gradient(#333933, #222922);
        border-color: #393;
        box-shadow: 0 0 5px rgba(0, 255, 0, .2), inset 0 0 5px rgba(0, 255, 0, .1), 0 2px 0 #000;
        color: #efe;
        outline: none;
    }

    .webdesigntuts-workshop input:focus::-webkit-input-placeholder {
        color: #efe;
    }

    .webdesigntuts-workshop input:focus:-moz-placeholder {
        color: #efe;
    }

    .webdesigntuts-workshop button {
        background: #222;
        background: -webkit-linear-gradient(#333, #222);
        background: -moz-linear-gradient(#333, #222);
        background: -o-linear-gradient(#333, #222);
        background: -ms-linear-gradient(#333, #222);
        background: linear-gradient(#333, #222);
        -webkit-box-sizing: content-box;
        -moz-box-sizing: content-box;
        -o-box-sizing: content-box;
        -ms-box-sizing: content-box;
        box-sizing: content-box;
        border: 1px solid #444;
        border-left-color: #000;
        border-radius: 0 5px 5px 0;
        box-shadow: 0 2px 0 #000;
        color: #fff;
        float: left;
        font-family: 'Cabin', helvetica, arial, sans-serif;
        font-size: 13px;
        font-weight: 400;
        height: 28px;
        line-height: 40px;
        margin: 0;
        padding: 0;
        position: relative;
        text-shadow: 0 -1px 0 #000;
        width: 50px;
    }

    .webdesigntuts-workshop button:hover,
    .webdesigntuts-workshop button:focus {
        background: #292929;
        background: -webkit-linear-gradient(#393939, #292929);
        background: -moz-linear-gradient(#393939, #292929);
        background: -o-linear-gradient(#393939, #292929);
        background: -ms-linear-gradient(#393939, #292929);
        background: linear-gradient(#393939, #292929);
        color: #;
        outline: none;
    }

    .webdesigntuts-workshop button:active {
        background: #292929;
        background: -webkit-linear-gradient(#393939, #292929);
        background: -moz-linear-gradient(#393939, #292929);
        background: -o-linear-gradient(#393939, #292929);
        background: -ms-linear-gradient(#393939, #292929);
        background: linear-gradient(#393939, #292929);
        box-shadow: 0 1px 0 #000, inset 1px 0 1px #222;
        top: 1px;
    }

    @-webkit-keyframes glow {
        0% {
            border-color: #393;
            box-shadow: 0 0 5px rgba(0, 255, 0, .2), inset 0 0 5px rgba(0, 255, 0, .1), 0 2px 0 #000;
        }
        100% {
            border-color: #6f6;
            box-shadow: 0 0 20px rgba(0, 255, 0, .6), inset 0 0 10px rgba(0, 255, 0, .4), 0 2px 0 #000;
        }
    }

    @-moz-keyframes glow {
        0% {
            border-color: #393;
            box-shadow: 0 0 5px rgba(0, 255, 0, .2), inset 0 0 5px rgba(0, 255, 0, .1), 0 2px 0 #000;
        }
        100% {
            border-color: #6f6;
            box-shadow: 0 0 20px rgba(0, 255, 0, .6), inset 0 0 10px rgba(0, 255, 0, .4), 0 2px 0 #000;
        }
    }

    @-o-keyframes glow {
        0% {
            border-color: #393;
            box-shadow: 0 0 5px rgba(0, 255, 0, .2), inset 0 0 5px rgba(0, 255, 0, .1), 0 2px 0 #000;
        }
        100% {
            border-color: #6f6;
            box-shadow: 0 0 20px rgba(0, 255, 0, .6), inset 0 0 10px rgba(0, 255, 0, .4), 0 2px 0 #000;
        }
    }

    @-ms-keyframes glow {
        0% {
            border-color: #393;
            box-shadow: 0 0 5px rgba(0, 255, 0, .2), inset 0 0 5px rgba(0, 255, 0, .1), 0 2px 0 #000;
        }
        100% {
            border-color: #6f6;
            box-shadow: 0 0 20px rgba(0, 255, 0, .6), inset 0 0 10px rgba(0, 255, 0, .4), 0 2px 0 #000;
        }
    }

    @keyframes glow {
        0% {
            border-color: #393;
            box-shadow: 0 0 5px rgba(0, 255, 0, .2), inset 0 0 5px rgba(0, 255, 0, .1), 0 2px 0 #000;
        }
        100% {
            border-color: #6f6;
            box-shadow: 0 0 20px rgba(0, 255, 0, .6), inset 0 0 10px rgba(0, 255, 0, .4), 0 2px 0 #000;

</style>

<?php
$visto = 0;
$count = mysql_query("SELECT * FROM buzon_sugerencia WHERE visto = '" . $visto . "'");
$numero = mysql_num_rows($count);

?>

<nav class="navbar navbar-inverse" role="banner" xmlns="http://www.w3.org/1999/html">
    <div class="container" style="width: 98.5%; max-width: 1500px">
        <div class="navbar-header" style="margin-right: 25px">

            <a class="navbar-brand" href="../vistas/indexAdministrador.php"><img src="../images/logo.png"
                                                                                 alt="logo"></a>
=======
    @import url(http://fonts.googleapis.com/css?family=Cabin:400);


.webdesigntuts-workshop:before {
    background: #444;
    background: -webkit-linear-gradient(left, #151515, #444, #151515);
    background: -moz-linear-gradient(left, #151515, #444, #151515);
    background: -o-linear-gradient(left, #151515, #444, #151515);
    background: -ms-linear-gradient(left, #151515, #444, #151515);
    background: linear-gradient(left, #151515, #444, #151515);
    top: 192px;
}

.webdesigntuts-workshop:after {
    background: #000;
    background: -webkit-linear-gradient(left, #151515, #000, #151515);  
    background: -moz-linear-gradient(left, #151515, #000, #151515); 
    background: -o-linear-gradient(left, #151515, #000, #151515);   
    background: -ms-linear-gradient(left, #151515, #000, #151515);  
    background: linear-gradient(left, #151515, #000, #151515);  
    top: 191px;
}

.webdesigntuts-workshop form {
    background: #111;
    background: -webkit-linear-gradient(#1b1b1b, #111);
    background: -moz-linear-gradient(#1b1b1b, #111);
    background: -o-linear-gradient(#1b1b1b, #111);
    background: -ms-linear-gradient(#1b1b1b, #111);
    background: linear-gradient(#1b1b1b, #111);
    border: 1px solid #000;
    border-radius: 5px;
    box-shadow: inset 0 0 0 1px #272727;
    display: inline-block;
    font-size: 0px;
    margin: 0px auto 0;
    padding: 10px;
    position: relative;
    z-index: 1;
}

.webdesigntuts-workshop input {
    background: #222;
    background: -webkit-linear-gradient(#333, #222);    
    background: -moz-linear-gradient(#333, #222);   
    background: -o-linear-gradient(#333, #222); 
    background: -ms-linear-gradient(#333, #222);    
    background: linear-gradient(#333, #222);    
    border: 1px solid #444;
    border-radius: 5px 0 0 5px;
    box-shadow: 0 2px 0 #000;
    color: #888;
    display: block;
    float: left;
    font-family: 'Cabin', helvetica, arial, sans-serif;
    font-size: 13px;
    font-weight: 400;
    height: 30px;
    margin: 0;
    padding: 0px;
    text-shadow: 0 -1px 0 #000;
    width: 150px;
}

.ie .webdesigntuts-workshop input {
    line-height: 40px;
}

.webdesigntuts-workshop input::-webkit-input-placeholder {
   color: #888;
}

.webdesigntuts-workshop input:-moz-placeholder {
   color: #888;
}

.webdesigntuts-workshop input:focus {
    -webkit-animation: glow 800ms ease-out infinite alternate;
    -moz-animation: glow 800ms ease-out infinite alternate;
    -o-animation: glow 800ms ease-out infinite alternate;
    -ms-animation: glow 800ms ease-out infinite alternate;
    animation: glow 800ms ease-out infinite alternate;
    background: #222922;
    background: -webkit-linear-gradient(#333933, #222922);
    background: -moz-linear-gradient(#333933, #222922);
    background: -o-linear-gradient(#333933, #222922);
    background: -ms-linear-gradient(#333933, #222922);
    background: linear-gradient(#333933, #222922);
    border-color: #393;
    box-shadow: 0 0 5px rgba(0,255,0,.2), inset 0 0 5px rgba(0,255,0,.1), 0 2px 0 #000;
    color: #efe;
    outline: none;
}

.webdesigntuts-workshop input:focus::-webkit-input-placeholder { 
    color: #efe;
}

.webdesigntuts-workshop input:focus:-moz-placeholder {
    color: #efe;
}

.webdesigntuts-workshop button {
    background: #222;
    background: -webkit-linear-gradient(#333, #222);
    background: -moz-linear-gradient(#333, #222);
    background: -o-linear-gradient(#333, #222);
    background: -ms-linear-gradient(#333, #222);
    background: linear-gradient(#333, #222);
    -webkit-box-sizing: content-box;
    -moz-box-sizing: content-box;
    -o-box-sizing: content-box;
    -ms-box-sizing: content-box;
    box-sizing: content-box;
    border: 1px solid #444;
    border-left-color: #000;
    border-radius: 0 5px 5px 0;
    box-shadow: 0 2px 0 #000;
    color: #fff;
    float: left;
    font-family: 'Cabin', helvetica, arial, sans-serif;
    font-size: 13px;
    font-weight: 400;
    height: 28px;
    line-height: 40px;
    margin: 0;
    padding: 0;
    position: relative;
    text-shadow: 0 -1px 0 #000;
    width: 50px;
}   

.webdesigntuts-workshop button:hover,
.webdesigntuts-workshop button:focus {
    background: #292929;
    background: -webkit-linear-gradient(#393939, #292929);  
    background: -moz-linear-gradient(#393939, #292929); 
    background: -o-linear-gradient(#393939, #292929);   
    background: -ms-linear-gradient(#393939, #292929);  
    background: linear-gradient(#393939, #292929);
    color: #;
    outline: none;
}

.webdesigntuts-workshop button:active {
    background: #292929;
    background: -webkit-linear-gradient(#393939, #292929);
    background: -moz-linear-gradient(#393939, #292929);
    background: -o-linear-gradient(#393939, #292929);
    background: -ms-linear-gradient(#393939, #292929);
    background: linear-gradient(#393939, #292929);
    box-shadow: 0 1px 0 #000, inset 1px 0 1px #222;
    top: 1px;
}

@-webkit-keyframes glow {
    0% {
        border-color: #393;
        box-shadow: 0 0 5px rgba(0,255,0,.2), inset 0 0 5px rgba(0,255,0,.1), 0 2px 0 #000;
    }   
    100% {
        border-color: #6f6;
        box-shadow: 0 0 20px rgba(0,255,0,.6), inset 0 0 10px rgba(0,255,0,.4), 0 2px 0 #000;
    }
}

@-moz-keyframes glow {
    0% {
        border-color: #393;
        box-shadow: 0 0 5px rgba(0,255,0,.2), inset 0 0 5px rgba(0,255,0,.1), 0 2px 0 #000;
    }   
    100% {
        border-color: #6f6;
        box-shadow: 0 0 20px rgba(0,255,0,.6), inset 0 0 10px rgba(0,255,0,.4), 0 2px 0 #000;
    }
}

@-o-keyframes glow {
    0% {
        border-color: #393;
        box-shadow: 0 0 5px rgba(0,255,0,.2), inset 0 0 5px rgba(0,255,0,.1), 0 2px 0 #000;
    }   
    100% {
        border-color: #6f6;
        box-shadow: 0 0 20px rgba(0,255,0,.6), inset 0 0 10px rgba(0,255,0,.4), 0 2px 0 #000;
    }
}

@-ms-keyframes glow {
    0% {
        border-color: #393;
        box-shadow: 0 0 5px rgba(0,255,0,.2), inset 0 0 5px rgba(0,255,0,.1), 0 2px 0 #000;
    }   
    100% {
        border-color: #6f6;
        box-shadow: 0 0 20px rgba(0,255,0,.6), inset 0 0 10px rgba(0,255,0,.4), 0 2px 0 #000;
    }
}

@keyframes glow {
    0% {
        border-color: #393;
        box-shadow: 0 0 5px rgba(0,255,0,.2), inset 0 0 5px rgba(0,255,0,.1), 0 2px 0 #000;
    }   
    100% {
        border-color: #6f6;
        box-shadow: 0 0 20px rgba(0,255,0,.6), inset 0 0 10px rgba(0,255,0,.4), 0 2px 0 #000;

</style>
<nav class="navbar navbar-inverse" role="banner" xmlns="http://www.w3.org/1999/html">
    <div class="container">
        <div class="navbar-header">

>>>>>>> fcb1717f91a1249180f89b0b43f43efeaa517a32
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
<<<<<<< HEAD
        </div>


        <div class="collapse navbar-collapse navbar-right">
            <ul class="nav navbar-nav">
                <li><a href="../vistas/indexAdministrador.php">Inicio</a></li>
=======
            <a class="navbar-brand" href="../vistas/indexAdministrador.php"><img src="../images/logo.png" alt="logo"></a>

        </div>
     
        <div class="collapse navbar-collapse navbar-right">
            <ul class="nav navbar-nav">
                <li ><a href="../vistas/indexAdministrador.php">Inicio</a></li>
>>>>>>> fcb1717f91a1249180f89b0b43f43efeaa517a32
                <li><a href="../vistas/listaTemas.php">Lista de Tema</a></li>
                <li><a href="../vistas/crearTema.php">Crear Tema</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Administrar <i
                            class="fa fa-angle-down"></i>
<<<<<<< HEAD
                    </a>
=======
                            </a>
>>>>>>> fcb1717f91a1249180f89b0b43f43efeaa517a32
                    <ul class="dropdown-menu">
                        <li><a href="../vistas/listaUsuarios.php">Usuarios</a></li>
                        <li><a href="../vistas/listaCategorias.php">Categorias</a></li>
                        <li><a href="../vistas/reportes.php">Reportes</a></li>
                        <li><a href="../vistas/bitacora.php">Bitacora</a></li>
<<<<<<< HEAD
                        <li><a href="../vistas/buzonSugerencias.php">Buzón De Sugerencias<br><center><?= $numero ?> Nuevos Mensajes</center></a></li>
=======
>>>>>>> fcb1717f91a1249180f89b0b43f43efeaa517a32
                    </ul>
                </li>
                <li class="dropdown"><?php
                    $datos = mysql_query("SELECT * FROM usuarios WHERE id_usuario ='" . $_SESSION['id_usuario'] . "'");
                    $dat = mysql_fetch_array($datos);
                    if (is_null($dat[2])) { ?>
                        <div class="circular"
<<<<<<< HEAD
                             style="float:left; background-image:url(../images/avatar/default.jpg);background-position:50% 50%; background-size:100% 100%;background-repeat: no-repeat;"></div>
=======
                             style="float:left; background-image:url(../images/avatar/default.jpg);background-position:50% 50%; background-size:auto auto;background-repeat: no-repeat;"></div>
>>>>>>> fcb1717f91a1249180f89b0b43f43efeaa517a32

                    <? } else { ?>
                        <div class="circular"
                             style="position:relative;float:left; background-image:url(../images/avatar/<?= $dat[0] ?>/<?= $dat[2] ?>);background-position:50% 50%; background-size:auto 100%;background-repeat: no-repeat; background-color: black"></div>
                    <? } ?>
                    <a href="#" class="dropdown-toggle"
                       data-toggle="dropdown"><? echo " " . $_SESSION['nombre']; ?> <? echo "" . $_SESSION['apellido']; ?>
                        <i class="fa fa-angle-down"></i></a>
                    <ul class="dropdown-menu">
<<<<<<< HEAD
                        <li><a href="../vistas/usuarioTemas.php?id=<?= $_SESSION['id_usuario'] ?>">Mis Temas</a></li>
                        <li><a href="../vistas/perfil.php?id=<?= $_SESSION['id_usuario'] ?>">Perfil</a></li>
                        <li><a href="../vistas/contacto.php">Contacto</a></li>
                        <li><a href="../complementos/logout.php"><i class="fa fa-sign-out"></i> Cerrar Sesión</a></li>
                    </ul>
                </li>
            </ul>
        </div>

        <div class="navbar-header" style="float: left">
            <section class="webdesigntuts-workshop">
                <form action="" method="">
                    <input type="Buscar" placeholder="Buscar...">
                    <button style="background-image: url(../images/buscar.png); background-position: 50% center; background-size: auto 100%; background-repeat: no-repeat "></button>
                </form>
            </section>
        </div>
    </div>
</nav>



=======
                        <li><a href="../vistas/misTemas.php">Mis Temas</a></li>
                        <li><a href="../vistas/perfil.php">Perfil</a></li>
                        <li><a href="../complementos/logout.php"><i class="fa fa-sign-out"></i> Cerrar Sesión</a></li>
                    </ul>
                </li>
                <section class="webdesigntuts-workshop">
    <form action="" method="">          
        <input type="Buscar" placeholder="Buscar...">               
        <button class="fa fa-search"></button>
    </form>
</section>
            </ul>
        </div>
    </div>
</nav>

>>>>>>> fcb1717f91a1249180f89b0b43f43efeaa517a32
