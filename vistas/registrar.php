<?session_start();?>
<html>
<?php include("../partes/head.php"); ?>
<body>
<?php include("../partes/header.php"); 
if(!isset($_SESSION['usuario_nombre'])){?>

<div class="container   animated fadeInRight" style="max-width: 550px">
    <div class="row subtitle">
        <h1>
            <center>
                Registrate
            </center>
        </h1>
    </div>

    <p>Crea una cuenta para compartir..</p>

    <?php
    if (isset($_POST['enviar'])) {

        function valida_email($correo)
        {
            if (preg_match('/^[A-Za-z0-9-_.+%]+@[A-Za-z0-9-.]+\.[A-Za-z]{2,4}$/', $correo)) return true;
            else return false;
        }

        $sin_espacios = count_chars($_POST['usuario_nombre'], 1);
        if (!empty($sin_espacios[32])) {
            echo "El campo <em>usuario_nombre</em> no debe contener espacios en blanco. <a href='javascript:history.back();'>Reintentar</a>";
        } elseif (empty($_POST['usuario_nombre'])) {
            echo "No haz ingresado tu usuario. <a href='javascript:history.back();'>Reintentar</a>";
        } elseif (empty($_POST['nombre'])) {
            echo "No haz ingresado tu nombre. <a href='javascript:history.back();'>Reintentar</a>";
        } elseif (empty($_POST['apellido'])) {
            echo "No haz ingresado tu apellido. <a href='javascript:history.back();'>Reintentar</a>";
        } elseif (empty($_POST['usuario_clave'])) {
            echo "No haz ingresado contraseña. <a href='javascript:history.back();'>Reintentar</a>";
        } elseif ($_POST['usuario_clave'] != $_POST['usuario_clave_conf']) {
            echo "Las contraseñas ingresadas no coinciden. <a href='javascript:history.back();'>Reintentar</a>";
        } elseif (!valida_email($_POST['usuario_email'])) {
            echo "El email ingresado no es válido. <a href='javascript:history.back();'>Reintentar</a>";
        } else {

            $usuario_nombre = mysql_real_escape_string($_POST['usuario_nombre']);
            $nombre = mysql_real_escape_string($_POST['nombre']);
            $apellido = mysql_real_escape_string($_POST['apellido']);
            $usuario_clave = mysql_real_escape_string($_POST['usuario_clave']);
            $usuario_email = mysql_real_escape_string($_POST['usuario_email']);

            $sql = mysql_query("SELECT usuario_nombre FROM usuarios WHERE usuario_nombre='" . $usuario_nombre . "'");
            if (mysql_num_rows($sql) > 0) {
                echo "El nombre usuario elegido ya ha sido registrado anteriormente. <a href='javascript:history.back();'>Reintentar</a>";
            } else {
                $usuario_clave = md5($usuario_clave); // encriptamos la contraseña ingresada con md5
                // ingresamos los datos a la BD
                $reg = mysql_query("INSERT INTO usuarios (usuario_nombre, nombre, apellido, usuario_clave, usuario_email, fecha_reg) VALUES ('" . $usuario_nombre . "', '" . $nombre . "', '" . $apellido . "', '" . $usuario_clave . "', '" . $usuario_email . "', NOW())");

                $consult = mysql_query("SELECT id_usuario FROM usuarios WHERE usuario_nombre = '" . $usuario_nombre . "'");
                $cons = mysql_fetch_array($consult);

                $modulo = "Resgistrarse";
                $accion = "Nuevo Usuario: " . $usuario_nombre . "";

                $bit = mysql_query("INSERT INTO bitacora (id_usuario, fecha, modulo, accion) VALUES ('" . $cons[0] . "', NOW(),'" . $modulo . "' , '" . $accion . "')");
                if ($bit) {
                    echo "Datos ingresados correctamente.";
                } else {
                    echo "ha ocurrido un error y no se registraron los datos.";
                }

                if ($reg) {
                    echo "Datos ingresados correctamente.  <a class=\"btn-primary btn-xs\" href=\"acceso.php\">Inicia Sesión</a>";
                } else {
                    echo "ha ocurrido un error y no se registraron los datos.";
                }

            }
        }
    } else {
        ?>
        <form action="<?= $_SERVER['PHP_SELF'] ?>" method="post">
            <div class="form-group">
                <input type="text" name="usuario_nombre" class="form-control" placeholder="Nombre de Usuario"
                       maxlength="15" minlength="6" required="">
            </div>
            <div class="form-group">
                <input type="text" name="nombre" class="form-control" placeholder="Nombre" maxlength="15"
                       minlength="3" required="">
            </div>
            <div class="form-group">
                <input type="text" name="apellido" class="form-control" placeholder="Apellido" maxlength="15"
                       minlength="3" required="">
            </div>
            <div class="form-group">
                <input type="password" name="usuario_clave" class="form-control" placeholder="Contraseña"
                       maxlength="15" minlength="6" required="">
            </div>
            <div class="form-group">
                <input type="password" name="usuario_clave_conf" class="form-control"
                       placeholder="Confirmar Contraseña" maxlength="15" minlength="6" required="">
            </div>
            <div class="form-group">
                <input type="email" name="usuario_email" class="form-control" placeholder="Correo Electrónico"
                       maxlength="50" minlength="6" required="">
            </div>
            <input type="submit" name="enviar" value="Registrar" class="btn-primary">

            </input><input class="btn-info" style="background-color: dimgrey; border-color: dimgrey;" type="reset" value="Limpiar"/>

            <p class="text-muted text-center">
                <small>Ya tienes una cuenta?</small>
            </p>
            <a class="btn-primary btn-xs" href="acceso.php">Inicia Sesión</a>
        </form>
    <?php } ?>
</div>

<?php
}else{ ?>
<script>
  window.location="../vistas/index.php";
</script><?
}
include("../partes/footer.php");
include("../partes/scripts.php");
?>
</body>
</html>
