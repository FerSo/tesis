<?php session_start(); ?>
<html>
<? include("../partes/head.php"); ?>
<body>
<?
include("../partes/header.php");
if (isset($_SESSION['usuario_nombre'])) {
    if ($_SESSION['nivel'] == '0') {
    ?>
        <br><br>
        <div class="container">

            <div class="container animated fadeInRight">
                <div class="row subtitle">
                    <h1>
                        <center>Buzón de Sugerencias</center>
                    </h1>
                </div>
                <div class="col-md-12">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="form-group">

                                        <table class="table">
                                            <thead>
                                            <tr style="border-bottom: 3px; border-top: 3px">
                                                <th style='text-align: center'>Ver Mensaje</th>
                                                <th></th>
                                                <th>Usuario</th>
                                                <th>Asunto</th>
                                                <th>Fecha</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $select = mysql_query("SELECT * FROM buzon_sugerencia ORDER BY fecha DESC");
                                            while ($row = mysql_fetch_row($select)) {
                                                $user = mysql_query("SELECT usuario_nombre FROM usuarios WHERE id_usuario = $row[1]");
                                                $name = mysql_fetch_row($user);
                                                $user = mysql_query("SELECT avatar FROM usuarios WHERE id_usuario = $row[1]");
                                                $url = mysql_fetch_row($user);
                                                $visto = mysql_query("SELECT visto FROM buzon_sugerencia WHERE id_sugerencia = '" . $row[0] . "'");
                                                $no_leido = mysql_fetch_row($visto);

                                                $time = date("d M Y - g:i a", strtotime($row[5]));
                                                if ( $no_leido[0] == 1) {
                                                    echo "<tr>";
                                                }else{
                                                    echo "<tr style='background-color: rgba(255,255,255,0.15)'>";
                                                }

                                                echo "<th style='text-align: center'><a title='Ver Todo' class=\"btn-primary btn-xs fa fa-eye\" href=\"sugerencia.php?id=$row[0]\"></a></th>";
                                                if (is_null($url[0])) {
                                                    echo "<th><div class=\"circulares\" style=\"float:left; background-image:url(../images/avatar/default.jpg);background-position:50% 50%; background-size:auto 100%;background-repeat: no-repeat;\"></div></th>";
                                                } else {
                                                    echo "<th><div class=\"circulares\" style=\"position:relative;float:left; background-image:url(../images/avatar/$row[1]/$url[0]);background-position:50% 50%; background-size:auto 100%;background-repeat: no-repeat; background-color: black\"></div></th>";
                                                }
                                                echo "<th>$name[0]</th>";
                                                echo "<th>$row[2]</th>";
                                                echo "<th>$time</th>";
                                                echo "</tr>";
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br><br><br><br><br><br>
        <?php

    } elseif ($_SESSION['nivel'] == '1') {
        echo "
        <br><br>
            <div class=\"col-lg-4 col-lg-offset-4 center animated fadeInRight\">

            <div class=\"row subtitle\">
                <h1>
                    Error
                </h1>
            </div>
        Estás accediendo a una página restringida, solo los administradores pueden acceder.<br/><br/>
        <a class='btn-primary' href='javascript:history.back();'>regresar</a>
        </div>
        </div>";
    }
} else {

    echo "
        <br><br>
            <div class=\"col-lg-4 col-lg-offset-4 center animated fadeInRight\">
            <div class=\"row subtitle\">
                <h1>
                    Error
                </h1>
            </div>
        Estás accediendo a una página restringida, solo los administradores pueden acceder.<br/><br/>
        <a class='btn-primary' href='javascript:history.back();'>regresar</a>
        </div>
        </div>";
}?>
</body>
<?
include("../partes/footer.php");
include("../partes/scriptsVistas.php");
?>
</html>