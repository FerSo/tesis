<?php
session_start();
include("../partes/headVistas.php");
include('../complementos/acceso_db.php'); // incluímos los datos de acceso a la BD
// comprobamos que se haya iniciado la sesión
if (isset($_SESSION['usuario_nombre'])) {
    ?>
    <html>
    <head>

        <title> Foros Informáticos</title>
    </head>
    <body>
    <?php if ($_SESSION['nivel'] == '0') {
        include("../partes/barraAdmin.php");

    } else {
        include("../partes/barraUsu.php");
    }
    ?>


    <br><br>
    <div class="container">        
            <div class="row">
                <div class="col-md-12">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <div class="ibox-content">
                                <div class="container   animated fadeInRight">
                                    <div class="row subtitle">
                                    <h1><center>Mis Temas</center></h1>
									</div>
                                    <div class="form-group">
                                        <table class="table table-hover">

                                            <thead>
                                            <tr>
                                                <th>Titulo de foro</th>
                                                <th>fecha de publicacion</th>
                                                <th>Categoria</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php

                                            $result = mysql_query("SELECT * FROM foro");
                                            while ($row = mysql_fetch_row($result)) {

                                                $categoria = mysql_query("SELECT nombre_categoria FROM categorias WHERE id_categoria = $row[5]");
                                                $category = mysql_fetch_row($categoria);
                                                if ($_SESSION['id_usuario'] == $row[1]) {
													
												$time = date("d M Y - g:i a", strtotime($row[4]));

                                                    echo "<tr>";
                                                    echo "<th><a href=\"Tema.php?id=$row[0]\">$row[2]</a></th>";
                                                    echo "<th><a href=\"Tema.php?id=$row[0]\">$time</a></th>";
                                                    echo "<th><a href=\"Tema.php?id=$row[0]\">$category[0]</a></th>";
                                                    echo "</tr>";

                                                }
                                            }

                                            ?>


                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <?php
    include("../partes/footer.php");
    include("../partes/scriptsVistas.php");
    include("../partes/scripts.php");
    ?>
    </body>
    </html>

    <?php
} else {
    include("../partes/barracomprobar.php");
    echo "
        <br><br>
        <div class='center'>
        Estás accediendo a una página restringida, para ver su contenido debes estar registrado.<br/><br/>

        <a class='btn-primary' href='acceso.php'>Ingresar</a> / <a class='btn-primary'   href='registrar.php'>Registrarme</a>
        
        </div>";
    include("../partes/footer.php");
    include("../partes/scripts.php");
    include("../partes/scriptsVistas.php");
}
?>