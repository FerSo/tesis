<?php
session_start();
include("../partes/headVistas.php");
include('../complementos/acceso_db.php'); // incluímos los datos de acceso a la BD
// comprobamos que se haya iniciado la sesión
if (isset($_SESSION['usuario_nombre'])) {
    if ($_SESSION['nivel'] == '0') {
        ?>
        <html>
        <head>
            <?php include("../partes/headVistas.php"); ?>
            <title> Foros Informáticos</title>
        </head>
        <body>
        <?php include("../partes/barraAdmin.php"); ?>
        <br><br>


            <div class="col-lg-4 col-lg-offset-4 center animated fadeInRight">
                <div class="row subtitle">
                    <h1>
                        <center> Nuevo Administrador
                            <center>
                    </h1>
                </div>
                <div>
                    <div>
                    </div>


                    <?php
                    include('../complementos/acceso_db.php'); // incluimos el archivo de conexión a la Base de Datos
                    if (isset($_POST['enviar'])) { // comprobamos que se han enviado los datos desde el formulario
                        // creamos una función que nos parmita validar el email
                        function valida_email($correo)
                        {
                            if (preg_match('/^[A-Za-z0-9-_.+%]+@[A-Za-z0-9-.]+\.[A-Za-z]{2,4}$/', $correo)) return true;
                            else return false;
                        }

                        // Procedemos a comprobar que los campos del formulario no estén vacíos
                        $sin_espacios = count_chars($_POST['usuario_nombre'], 1);
                        if (!empty($sin_espacios[32])) { // comprobamos que el campo usuario_nombre no tenga espacios en blanco
                            echo "El campo <em>usuario_nombre</em> no debe contener espacios en blanco. <a href='javascript:history.back();'>Reintentar</a>";
                        } elseif (empty($_POST['usuario_nombre'])) { // comprobamos que el campo usuario_nombre no esté vacío
                            echo "No haz ingresado tu usuario. <a href='javascript:history.back();'>Reintentar</a>";
                        } elseif (empty($_POST['nombre'])) { // comprobamos que el campo nombre no esté vacío
                            echo "No haz ingresado tu nombre. <a href='javascript:history.back();'>Reintentar</a>";
                        } elseif (empty($_POST['apellido'])) { // comprobamos que el campo apellido no esté vacío
                            echo "No haz ingresado tu apellido. <a href='javascript:history.back();'>Reintentar</a>";
                        } elseif (empty($_POST['usuario_clave'])) { // comprobamos que el campo usuario_clave no esté vacío
                            echo "No haz ingresado contraseña. <a href='javascript:history.back();'>Reintentar</a>";
                        } elseif ($_POST['usuario_clave'] != $_POST['usuario_clave_conf']) { // comprobamos que las contraseñas ingresadas coincidan
                            echo "Las contraseñas ingresadas no coinciden. <a href='javascript:history.back();'>Reintentar</a>";
                        } elseif (!valida_email($_POST['usuario_email'])) { // validamos que el email ingresado sea correcto
                            echo "El email ingresado no es válido. <a href='javascript:history.back();'>Reintentar</a>";
                        } else {

                            // "limpiamos" los campos del formulario de posibles códigos maliciosos

                            $usuario_nombre = mysql_real_escape_string($_POST['usuario_nombre']);
                            $nombre = mysql_real_escape_string($_POST['nombre']);
                            $apellido = mysql_real_escape_string($_POST['apellido']);
                            $usuario_clave = mysql_real_escape_string($_POST['usuario_clave']);
                            $usuario_email = mysql_real_escape_string($_POST['usuario_email']);

                            // comprobamos que el usuario ingresado no haya sido registrado antes

                            $sql = mysql_query("SELECT usuario_nombre FROM usuarios WHERE usuario_nombre='" . $usuario_nombre . "'");
                            if (mysql_num_rows($sql) > 0) {
                                echo "El nombre usuario elegido ya ha sido registrado anteriormente. <a href='javascript:history.back();'>Reintentar</a>";
                            } else {
                                $usuario_clave = md5($usuario_clave); // encriptamos la contraseña ingresada con md5
                                // ingresamos los datos a la BD
                                $reg = mysql_query("INSERT INTO usuarios (usuario_nombre, nombre, apellido, usuario_clave, usuario_email, nivel, fecha_reg) VALUES ('" . $usuario_nombre . "', '" . $nombre . "', '" . $apellido . "', '" . $usuario_clave . "', '" . $usuario_email . "', 0, NOW())");
                                if ($reg) {
                                    echo "Datos ingresados correctamente.";
                                } else {
                                    echo "ha ocurrido un error y no se registraron los datos.";
                                }
                            }
                        }
                    } else {
                        ?>
                        <form action="<?= $_SERVER['PHP_SELF'] ?>" method="post">
                            <div class="form-group">
                                <input type="text" name="usuario_nombre" class="form-control"
                                       placeholder="Nombre de Usuario" maxlength="15" minlength="6" required="">
                            </div>
                            <div class="form-group">
                                <input type="text" name="nombre" class="form-control" placeholder="Nombre"
                                       maxlength="15" minlength="6" required="">
                            </div>
                            <div class="form-group">
                                <input type="text" name="apellido" class="form-control" placeholder="Apellido"
                                       maxlength="15" minlength="6" required="">
                            </div>
                            <div class="form-group">
                                <input type="password" name="usuario_clave" class="form-control"
                                       placeholder="Contraseña" maxlength="15" minlength="6" required="">
                            </div>
                            <div class="form-group">
                                <input type="password" name="usuario_clave_conf" class="form-control"
                                       placeholder="Confirmar Contraseña" maxlength="15" minlength="6" required="">
                            </div>
                            <div class="form-group">
                                <input type="email" name="usuario_email" class="form-control"
                                       placeholder="Correo Electrónico" maxlength="50" minlength="6" required="">
                            </div>
                            <input type="submit" name="enviar" value="Registrar" class=" btn-primary"></input><input
                                class="btn" type="reset" value="Limpiar"/>
                        </form>
                        <?php
                    }
                    ?>
                </div>
            </div>
        
        </div>

        <?php include("../partes/footer.php"); ?>
        <?php include("../partes/scriptsVistas.php"); ?>
        </body>
        </html>
        <?php
    } else {

        include("../partes/barraComprobar.php");
        echo "
        <br><br>
        <div class='center'>
        Estás accediendo a una página restringida, debes ser administrador para ver su contenido<br/><br/>
    <a class='btn-primary' href='javascript:history.back();'>regresar</a>
               
        </div>";
        include("../partes/footer.php");
        include("../partes/scriptsVistas.php");
    }
}
?>