<?php session_start(); ?>
<html>
<?php include("../partes/head.php"); ?>
<body>
<?php include("../partes/header.php");

if (!isset($_SESSION['usuario_nombre'])){

 ?>

<div class="col-lg-4 col-lg-offset-4 center animated fadeInRight">
    <div class="row subtitle">
        <h1>
            Bienvenido a Foros Informáticos
        </h1>
    </div>
    <p>Inicia Sesión para compartir.</p>
    <form action="../complementos/comprobar.php" method="post">
        <div class="form-group">
            <input type="text" class="form-control " name="usuario_nombre" placeholder="Nombre de usuario"
            >
        </div>
        <div class="form-group">
            <input type="password" class="form-control" name="usuario_clave" placeholder="Contraseña"
            >
        </div>
        <input type="submit" class="btn-primary" name="enviar" value="Iniciar Sesión">
        </input>
        <p class="text-muted text-center">
            <small>no tienes una cuenta?</small>
        </p>
        <a class="btn-primary btn-xs" href="registrar.php">Crea una cuenta.</a>
    </form>
</div>
<?
}else{
    ?>
<script>window.location="../vistas/index.php"</script>
<?
}
?>

<?php include("../partes/footer.php"); ?>

</body>
</html>
