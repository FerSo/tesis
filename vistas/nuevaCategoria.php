<?php
session_start();
include("../partes/head.php");
include('../complementos/acceso_db.php'); // incluímos los datos de acceso a la BD
// comprobamos que se haya iniciado la sesión
if (isset($_SESSION['usuario_nombre'])) {
    if ($_SESSION['nivel'] == '0') {
        ?>
        <html>
        <head>
            <?php include("../partes/headVistas.php"); ?>
            <title> Foros Informáticos</title>
        </head>
        <body>
        <?php include("../partes/barraAdmin.php"); ?>


        <?php include("../partes/footer.php"); ?>
        <?php include("../partes/scriptsVistas.php"); ?>
        </body>
        </html>
        <?php
    } else {

        echo "
        <br><br>
        <div class='center'>
        Estás accediendo a una página restringida, debes ser administrador para ver su contenido<br/><br/>
    <a class='btn-primary' href='javascript:history.back();'>regresar</a>
       
        
        </div>";
        include("../partes/footer.php");
        include("../partes/scriptsVistas.php");
    }
}
?>