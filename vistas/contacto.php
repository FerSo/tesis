<?php
session_start();
include("../partes/headVistas.php"); ?>
    <script src="../js/nicEdit-latest.js" type="text/javascript"></script>
    <script type="text/javascript">bkLib.onDomLoaded(nicEditors.allTextAreas);</script>
<?
include('../complementos/acceso_db.php'); // incluímos los datos de acceso a la BD
// comprobamos que se haya iniciado la sesión
if (isset($_SESSION['usuario_nombre'])) {
    ?>
    <html>
    <head>

        <title> Foros Informáticos</title>
    </head>
    <body>
    <?php if ($_SESSION['nivel'] == '0') {
        include("../partes/barraAdmin.php");

    } else {
        include("../partes/barraUsu.php");
    }
    ?>
    <br><br>

    <div class="container   animated fadeInRight">

        <div class="row subtitle">
            <h1>
                <center><? echo "Hola " . $_SESSION['nombre'] . " Contactanos por acá."; ?>
                    <center>
            </h1>
        </div>
        <label>Si quieres darnos una Sugerencia o Preguntar algo no dudes en contactanos.</label>
        <?php
        include('../complementos/acceso_db.php');
        if (isset($_POST['enviar'])) {


            $sin_espacios = count_chars($_POST['usuario_nombre'], 1);
            if (!empty($sin_espacios[32])) {
                echo "El campo <em>usuario_nombre</em> no debe contener espacios en blanco. <a href='javascript:history.back();'>Reintentar</a>";
            } elseif (empty($_POST['asunto'])) {
                echo "No haz ingresado algun titulo. <a href='javascript:history.back();'>Reintentar</a>";

            } elseif (empty($_POST['mensaje'])) {
                echo "No haz ingresado un contenido. <a href='javascript:history.back();'>Reintentar</a>";

            } else {

                $asunto = mysql_real_escape_string($_POST['asunto']);
                $mensaje = mysql_real_escape_string($_POST['mensaje']);



                $reg = mysql_query("INSERT INTO buzon_sugerencia (id_usuario, asunto,mensaje, fecha) VALUES ('" . $_SESSION['id_usuario'] . "','" . $asunto . "', '" . $mensaje . "', NOW())");
                if ($reg) {
                    echo "Mensaje Enviado Exitosamente.";
                } else {
                    echo "ha ocurrido un error y no se registraron los datos.";
                }

                $modulo = "Contacto";
                $accion = "A Enviado Un Mensaje";
                $bit = mysql_query("INSERT INTO bitacora (id_usuario, fecha, modulo, accion) VALUES ('" . $_SESSION['id_usuario'] . "', NOW(),'" . $modulo . "' , '" . $accion . "')");
                if ($bit) {
                    echo "Datos ingresados correctamente.";
                } else {
                    echo "ha ocurrido un error y no se registraron los datos.";
                }

            }
        } else {
            ?>
            <form action="<?= $_SERVER['PHP_SELF'] ?>" method="post">

                <div class="form-group">
                    <input type="text" name="asunto" class="form-control" placeholder="Asunto" maxlength="100"
                           minlength="6" required="">
                </div>

                <div class="form-group">
                    <div class="form-group"><label>Mensaje</label>
                        <textarea class="form-control" name="mensaje" id="details" cols="20" rows="10"></textarea>
                    </div>
                </div>


                <input type="submit" name="enviar" value="Enivar" class=" btn-primary pull-right">
                <input class="btn pull-clear" type="reset"
                       value="Limpiar"/>
            </form>
            <?php
        }
        ?>
    </div>
    </div>
    <br><br><br><br><br><br>
    <?php include("../partes/footer.php");
    include("../partes/scripts.php"); ?>
    </body>
    </html>

    <?php
} else {
    include("../partes/barracomprobar.php");
    echo "
        <br><br>


    <div class=\"col-lg-4 col-lg-offset-4 center animated fadeInRight\">

    <div class=\"row subtitle\">
        <h1>
            Error
        </h1>
    </div>

    
        Estás accediendo a una página restringida, para ver su contenido debes estar registrado, y si ya tienes una cuenta, puedes acceder.<br/><br/>

        <a class='btn-primary' href='../acceso.php'>Ingresar</a> / <a class='btn-primary'   href='../registrar.php'>Registrarme</a>

        </div>
        </div>";
    include("../partes/footer.php");
    include("../partes/scriptsVistas.php");
}
?>