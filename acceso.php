<?php
session_start();
include('complementos/acceso_db.php');
if (empty($_SESSION['usuario_nombre'])) { // comprobamos que las variables de sesiónn esatn vacias
    ?>
    <html>
    <head>
        <?php include("partes/head.php"); ?>
        <title> Foros Informáticos</title>
    </head>
    <body>
    <?php include("partes/barra.php"); ?>
    <br><br>

    <div class="col-lg-4 col-lg-offset-4 center animated fadeInRight">
        <div class="row subtitle">
            <h1>
                Bienvenido a Foros Informáticos
            </h1>
        </div>
        <p>Inicia Sesión para compartir.</p>
        <form action="complementos/comprobar.php" method="post">
            <div class="form-group">
                <input type="text" class="form-control " name="usuario_nombre" placeholder="Nombre de usuario"
                >
            </div>
            <div class="form-group">
                <input type="password" class="form-control" name="usuario_clave" placeholder="Contraseña"
                >
            </div>
            <input type="submit" class="btn-primary" name="enviar" value="Iniciar Sesión"></input>
            <p class="text-muted text-center">
                <small>no tienes una cuenta?</small>
            </p>
            <a class="btn-primary btn-xs" href="vistas/registrar.php">Crea una cuenta.</a>
        </form>
    </div>
    <br><br><br><br>
    <?php include("partes/footer.php"); ?>
    <?php include("partes/scripts.php"); ?>
    </body>
    </html>
    <?php
} else {
    if ($_SESSION['nivel'] == '1') {
        header("Location: vistas/indexUsuarios.php");
        ?>

        <?php
    } else {
        header("Location: vistas/indexAdministrador.php");
    }
}
?>


